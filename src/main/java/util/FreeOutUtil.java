package util;

import java.io.File;

import constant.Constant;

public class FreeOutUtil {

	public static String setProjectName(String codePath, String projectName) {
		return codePath.replaceAll(Constant.projectName, projectName);
	}
	
	public static String setWebClientName(String codePath, String projectName) {
		return codePath.replaceAll(Constant.projectName, projectName);
	}

	public static String checkAndMakeDir(String mkdir) {

		File path = new File(mkdir);

		if (!path.exists()) {
			path.mkdirs();
		}

		return mkdir;
	}
}
