package util;

import constant.ChildWindowConstant;
import constant.CodeConstant;
import constant.CodeFtlConstant;
import constant.Constant;
import constant.FreeMarkerConfig;
import entity.DatabaseModel;
import entity.EntityFieldModel;
import entity.MakeEntityModel;
import entity.TableConditionModel;
import entity.TableFiledModel;
import entity.TableNameAndType;
import entity.TablesQueryModel;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import javax.swing.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @ClassName CodeWriterUtil
 * @Author zrx
 * @Date 2020/8/4 11:54
 */
public class CodeWriterUtil {

	public static Template getTemplate(String ftlStr) throws IOException {
		Template template = FreeMarkerConfig.configuration.getTemplate(ftlStr);
		template.setEncoding(StandardCharsets.UTF_8.name());
		return template;
	}

	// 首字母大写 驼峰
	public static String captureName(String name) {
		String result = "";
		if (name.contains(".")) {
			String[] nameArr = name.split("\\.");
			for (String str : nameArr) {
				if (str.length() > 1) {
					result += str.substring(0, 1).toUpperCase() + str.substring(1);
				} else {
					result += str.substring(0, 1).toUpperCase();
				}
			}
		} else if (name.contains("_")) {
			String[] nameArr = name.split("\\_");
			for (String str : nameArr) {
				if (str.length() > 1) {
					result += str.substring(0, 1).toUpperCase() + str.substring(1);
				} else {
					result += str.substring(0, 1).toUpperCase();
				}
			}
		} else if (name.contains("-")) {
			String[] nameArr = name.split("-");
			for (String str : nameArr) {
				if (str.length() > 1) {
					result += str.substring(0, 1).toUpperCase() + str.substring(1);
				} else {
					result += str.substring(0, 1).toUpperCase();
				}
			}
		} else {
			if (name.length() > 1) {
				result = name.substring(0, 1).toUpperCase() + name.substring(1);
			} else {
				result = name.substring(0, 1).toUpperCase();
			}
		}
		return result;
	}

	/**
	 * 登录用户设置
	 *
	 * @param loginModel
	 * @param root
	 */
	public static void setLonginUser(String loginModel, Map<String, Object> root) {

		if ("静态用户".equals(loginModel)) {
			String condition = "";
			if (ChildWindowConstant.user1.size() != 0) {
				condition += "(\"" + ChildWindowConstant.user1.get(0) + "\"" + ".equals(user.getUserName()) && " + "\""
						+ ChildWindowConstant.user1.get(1) + "\"" + ".equals(user.getPassword()))" + " || ";
			}
			if (ChildWindowConstant.user2.size() != 0) {
				condition += "(\"" + ChildWindowConstant.user2.get(0) + "\"" + ".equals(user.getUserName()) && " + "\""
						+ ChildWindowConstant.user2.get(1) + "\"" + ".equals(user.getPassword()))" + " || ";
			}
			if (ChildWindowConstant.user3.size() != 0) {
				condition += "(\"" + ChildWindowConstant.user3.get(0) + "\"" + ".equals(user.getUserName()) && " + "\""
						+ ChildWindowConstant.user3.get(1) + "\"" + ".equals(user.getPassword()))" + " || ";
			}
			condition = condition.substring(0, condition.lastIndexOf("|") - 2);
			root.put(CodeConstant.userCondition, condition);
			root.put(CodeConstant.userNameFiled, "userName");
			root.put(CodeConstant.userPwdFiled, "password");
		} else if ("动态用户".equals(loginModel)) {
			root.put(CodeConstant.userTable, ChildWindowConstant.dynamicUserList.get(0));
			root.put(CodeConstant.userNameFiled, ChildWindowConstant.dynamicUserList.get(1));
			root.put(CodeConstant.userPwdFiled, ChildWindowConstant.dynamicUserList.get(2));
		}
	}

	/**
	 * 自定义实体
	 *
	 * @throws Exception
	 */
	public static void getMakeEntity(String outPathVal, String entityDir, Map<String, Object> root) throws Exception {
		// 自定义实体
		if (ChildWindowConstant.makeEntityModelMap.size() == 0) {
			return;
		}
		Template makeEntityTemplate = getTemplate(CodeFtlConstant.makeEntity_ftl);
		// 输出流
		for (Map.Entry<String, List<MakeEntityModel>> entry : ChildWindowConstant.makeEntityModelMap.entrySet()) {
			Writer makeEntityOut = null;
			try {
				String makeEntityName = entry.getKey();
				List<MakeEntityModel> makeEntityModels = entry.getValue();
				for (MakeEntityModel makeEntityModel : makeEntityModels) {
					if ("List".equals(makeEntityModel.getServiceType())) {
						root.put(CodeConstant.hasList, 1);
						break;
					}
				}
				root.put(CodeConstant.makeEntityName, makeEntityName);
				root.put(CodeConstant.MakeEntityModels, makeEntityModels);
				root.put(CodeConstant.makeEntityName_cn, ChildWindowConstant.makeEntityEngAndCn.get(makeEntityName));
				makeEntityOut = new OutputStreamWriter(new FileOutputStream(
						FreeOutUtil.checkAndMakeDir(outPathVal + entityDir) + captureName(makeEntityName) + ".java"),
						StandardCharsets.UTF_8);
				makeEntityTemplate.process(root, makeEntityOut);
				root.remove(CodeConstant.hasList);
				makeEntityOut.flush();
			} finally {
				if (makeEntityOut != null) {
					makeEntityOut.close();
				}
			}
		}
	}

	/**
	 * springboot用到的文件 yml 启动类等
	 *
	 * @throws Exception
	 */
	public static void getSpringbootCommon(String outPathVal, String resourcesDir,
										   String mainApplicationDir, String captureProjectNameVal,
										   String mainApplicationTestDir, String mvcConfigDir,
										   Map<String, Object> root, String frameWorkVal) throws Exception {
		// 自定义实体
		if (!"springBoot".equals(frameWorkVal)) {
			return;
		}
		// resourcesYmlOut输出流
		Writer resourcesYmlOut = null;
		// springboot的入口文件文件的输出流
		Writer mainApplicationOut = null;
		// springboot的入口测试文件文件的输出流
		Writer mainApplicationTestOut = null;
		// webMvcConfig
		Writer mvcConfigOut = null;
		// transactionAdviceConfig
		Writer transactionAdviceConfigOut = null;
		try {
			Template ymlTemplate = getTemplate(CodeFtlConstant.applicationYml_ftl);
			Template mainApplicationTemplate = getTemplate(CodeFtlConstant.mainApplication_ftl);
			Template mainApplicationTestTemplate = getTemplate(CodeFtlConstant.mainApplicationTest_ftl);
			Template transactionAdviceConfigTemplate = getTemplate(CodeFtlConstant.transactionAdviceConfig_ftl);
			Template mvcConfigTemplate = getTemplate(CodeFtlConstant.mvcConfig_ftl);
			// application.yml的输出文件
			resourcesYmlOut = new OutputStreamWriter(new FileOutputStream(
					FreeOutUtil.checkAndMakeDir(outPathVal + resourcesDir) + "application.yml"), StandardCharsets.UTF_8);
			// 生成yml
			ymlTemplate.process(root, resourcesYmlOut);
			// mainApplication的输出流
			mainApplicationOut = new OutputStreamWriter(
					new FileOutputStream(FreeOutUtil.checkAndMakeDir(outPathVal + mainApplicationDir)
							+ captureProjectNameVal + "Application.java"),
					StandardCharsets.UTF_8);
			// 生成 mainApplication
			mainApplicationTemplate.process(root, mainApplicationOut);
			// testApplication的输出流
			mainApplicationTestOut = new OutputStreamWriter(
					new FileOutputStream(FreeOutUtil.checkAndMakeDir(outPathVal + mainApplicationTestDir)
							+ captureProjectNameVal + "ApplicationTests.java"),
					StandardCharsets.UTF_8);
			// 生成 testApplication
			mainApplicationTestTemplate.process(root, mainApplicationTestOut);
			// mvcconfig的输出流
			mvcConfigOut = new OutputStreamWriter(
					new FileOutputStream(FreeOutUtil.checkAndMakeDir(outPathVal + mvcConfigDir) + "MvcConfig.java"),
					StandardCharsets.UTF_8);
			// 生成 mvcconfig
			mvcConfigTemplate.process(root, mvcConfigOut);
			// transactionAdviceConfig的输出流
			transactionAdviceConfigOut = new OutputStreamWriter(new FileOutputStream(
					FreeOutUtil.checkAndMakeDir(outPathVal + mvcConfigDir) + "TransactionAdviceConfig.java"),
					StandardCharsets.UTF_8);
			// 生成 transactionAdviceConfig
			transactionAdviceConfigTemplate.process(root, transactionAdviceConfigOut);
			//flush刷新流
			resourcesYmlOut.flush();
			mainApplicationOut.flush();
			mainApplicationTestOut.flush();
			mvcConfigOut.flush();
			transactionAdviceConfigOut.flush();
		} finally {
			if (resourcesYmlOut != null) {
				resourcesYmlOut.close();
			}
			if (mainApplicationOut != null) {
				mainApplicationOut.close();
			}
			if (mainApplicationTestOut != null) {
				mainApplicationTestOut.close();
			}
			if (mvcConfigOut != null) {
				mvcConfigOut.close();
			}
			if (transactionAdviceConfigOut != null) {
				transactionAdviceConfigOut.close();
			}
		}
	}

	/**
	 * springMVC用到的文件
	 *
	 * @throws Exception
	 */
	public static void getSpringMvcCommon(String outPathVal, String resourcesDir,
										  String projectNameVal, String htmlDir,
										  String settingsDir, String mvcConfigDir,
										  Map<String, Object> root, String frameWorkVal) throws Exception {
		// 自定义实体
		if (!"ssm".equals(frameWorkVal)) {
			return;
		}
		// mvcInterceptor
		Writer mvcInterceptorOut = null;
		// jdbc.properties
		Writer mvcJdbcPropertiesOut = null;
		// spring-mvc.xml
		Writer springMvcXmlOut = null;
		// spring-mybatis.xml
		Writer springMybatisOut = null;
		// web.xml
		Writer webXmlOut = null;
		// orgEclipseComponent
		Writer orgEclipseComponentOut = null;
		// orgEclipseComponent
		Writer manifestmfOut = null;
		try {
			Template mvcInterceptorTemplate = getTemplate(CodeFtlConstant.mvcInterceptor_ftl);
			Template mvcJdbcPropertiesTemplate = getTemplate(CodeFtlConstant.mvcJdbcProperties_ftl);
			Template mvcSpringMvcXmlTemplate = getTemplate(CodeFtlConstant.mvcSpringMvcXml_ftl);
			Template mvcSpringMybatisTemplate = getTemplate(CodeFtlConstant.mvcSpringMybatis_ftl);
			Template webXmlTemplate = getTemplate(CodeFtlConstant.webXml_ftl);
			Template orgEclipseComponentTemplate = getTemplate(CodeFtlConstant.orgEclipseComponent_ftl);
			Template manifestmfTemplate = getTemplate(CodeFtlConstant.manifestmf_ftl);
			String metaInfDir = FreeOutUtil.setProjectName(Constant.freeOutPath_METAINF_MVC, projectNameVal);
			// 创建meta_inf文件夹
			FreeOutUtil.checkAndMakeDir(outPathVal + metaInfDir);
			// mvcInterceptor的输出流
			mvcInterceptorOut = new OutputStreamWriter(
					new FileOutputStream(
							FreeOutUtil.checkAndMakeDir(outPathVal + mvcConfigDir) + "MVCInterceptor.java"),
					StandardCharsets.UTF_8);
			mvcInterceptorTemplate.process(root, mvcInterceptorOut);
			mvcJdbcPropertiesOut = new OutputStreamWriter(new FileOutputStream(
					FreeOutUtil.checkAndMakeDir(outPathVal + resourcesDir) + "jdbc.properties"), StandardCharsets.UTF_8);
			mvcJdbcPropertiesTemplate.process(root, mvcJdbcPropertiesOut);
			springMvcXmlOut = new OutputStreamWriter(
					new FileOutputStream(FreeOutUtil.checkAndMakeDir(outPathVal + resourcesDir) + "spring-mvc.xml"),
					StandardCharsets.UTF_8);
			mvcSpringMvcXmlTemplate.process(root, springMvcXmlOut);
			springMybatisOut = new OutputStreamWriter(
					new FileOutputStream(
							FreeOutUtil.checkAndMakeDir(outPathVal + resourcesDir) + "spring-mybatis.xml"),
					StandardCharsets.UTF_8);
			mvcSpringMybatisTemplate.process(root, springMybatisOut);
			webXmlOut = new OutputStreamWriter(
					new FileOutputStream(FreeOutUtil.checkAndMakeDir(outPathVal + htmlDir) + "web.xml"), StandardCharsets.UTF_8);
			webXmlTemplate.process(root, webXmlOut);
			orgEclipseComponentOut = new OutputStreamWriter(new FileOutputStream(
					FreeOutUtil.checkAndMakeDir(outPathVal + settingsDir) + "org.eclipse.wst.common.component"),
					StandardCharsets.UTF_8);
			orgEclipseComponentTemplate.process(root, orgEclipseComponentOut);
			manifestmfOut = new OutputStreamWriter(
					new FileOutputStream(FreeOutUtil.checkAndMakeDir(outPathVal + metaInfDir) + "MANIFEST.MF"),
					StandardCharsets.UTF_8);
			manifestmfTemplate.process(root, manifestmfOut);
			//flush刷新流
			mvcInterceptorOut.flush();
			mvcJdbcPropertiesOut.flush();
			springMvcXmlOut.flush();
			springMybatisOut.flush();
			webXmlOut.flush();
			orgEclipseComponentOut.flush();
			manifestmfOut.flush();
		} finally {
			if (mvcInterceptorOut != null) {
				mvcInterceptorOut.close();
			}
			if (mvcJdbcPropertiesOut != null) {
				mvcJdbcPropertiesOut.close();
			}
			if (springMvcXmlOut != null) {
				springMvcXmlOut.close();
			}
			if (springMybatisOut != null) {
				springMybatisOut.close();
			}
			if (webXmlOut != null) {
				webXmlOut.close();
			}
			if (orgEclipseComponentOut != null) {
				orgEclipseComponentOut.close();
			}
			if (manifestmfOut != null) {
				manifestmfOut.close();
			}
		}
	}

	/**
	 * 动态用户生成的文件（用户名密码需要连接数据库查询的情况）
	 *
	 * @throws Exception
	 */
	public static void getDynamicUser(String outPathVal, String sqlMapperDir,
									  String IServiceDir, String serviceDir,
									  String daoDir, String loginModel, Map<String, Object> root) throws Exception {
		// 自定义实体
		if (!"动态用户".equals(loginModel)) {
			return;
		}
		// 登录功能相关
		Writer userMapperOut = null;
		Writer IUserServiceOut = null;
		Writer userServiceOut = null;
		Writer IUserDaoOut = null;
		try {
			Template userMapperTemplate = getTemplate(CodeFtlConstant.userMapper_ftl);
			Template IUserServiceTemplate = getTemplate(CodeFtlConstant.IUserService_ftl);
			Template userServiceTemplate = getTemplate(CodeFtlConstant.userService_ftl);
			Template IUserDaoTemplate = getTemplate(CodeFtlConstant.IUserDao_ftl);
			userMapperOut = new OutputStreamWriter(new FileOutputStream(
					FreeOutUtil.checkAndMakeDir(outPathVal + sqlMapperDir) + "loginMapper.xml"), StandardCharsets.UTF_8);
			userMapperTemplate.process(root, userMapperOut);
			IUserServiceOut = new OutputStreamWriter(new FileOutputStream(
					FreeOutUtil.checkAndMakeDir(outPathVal + IServiceDir) + "ILoginService.java"), StandardCharsets.UTF_8);
			IUserServiceTemplate.process(root, IUserServiceOut);
			userServiceOut = new OutputStreamWriter(new FileOutputStream(
					FreeOutUtil.checkAndMakeDir(outPathVal + serviceDir) + "LoginService.java"), StandardCharsets.UTF_8);
			userServiceTemplate.process(root, userServiceOut);
			IUserDaoOut = new OutputStreamWriter(
					new FileOutputStream(FreeOutUtil.checkAndMakeDir(outPathVal + daoDir) + "ILoginDao.java"),
					StandardCharsets.UTF_8);
			IUserDaoTemplate.process(root, IUserDaoOut);
			//flush刷新流
			userMapperOut.flush();
			IUserServiceOut.flush();
			userServiceOut.flush();
			IUserDaoOut.flush();
		} finally {
			if (userMapperOut != null) {
				userMapperOut.close();
			}
			if (IUserServiceOut != null) {
				IUserServiceOut.close();
			}
			if (userServiceOut != null) {
				userServiceOut.close();
			}
			if (IUserDaoOut != null) {
				IUserDaoOut.close();
			}
		}
	}

	/**
	 * 公共代码的生成
	 *
	 * @throws Exception
	 */
	public static void getCommonCode(String outPathVal, String projectDir,
									 String utilDir, String themeVal,
									 String htmlDir, String webClientHtmlPath,
									 String daoDir, String resourcesDir, String sqlMapperDir,
									 String controllerDir, String mvcConfigDir, String entityDir, String constantDir,
									 String staticDir, String webClientJsConfigPath,
									 Map<String, Object> root) throws Exception {
		// pom文件的输出流
		Writer pomOut = null;
		// clpassPath文件的输出流
		Writer clpassPathOut = null;
		// .project文件的输出流
		Writer projectOut = null;
		// castutil
		Writer castUtilOut = null;
		// home.html
		Writer homeHtmlOut = null;
		// welcome.html
		Writer welcomeHtmlOut = null;
		// IBaseDao
		Writer IBaseDaoOut = null;
		// ExcelUtil
		Writer excelUtilOut = null;
		// logback
		Writer logbackOut = null;
		// mybatisConfig
		Writer mybatisConfigOut = null;
		// 登录功能相关
		Writer userControllerOut = null;
		Writer loginHtmlOut = null;
		Writer userModelOut = null;
		// 全局异常处理
		Writer allExceptionHandlerOut = null;
		// 日志切面
		Writer logAopAspectOut = null;
		// 日志工具类
		Writer loggerUtilOut = null;
		// 生成commonEntity
		Writer commonEntityOut = null;
		// 生成pageData
		Writer pageDataOut = null;
		// 生成请求公共返回的实体bean
		Writer commonResultOut = null;
		// resultConstant
		Writer resultConstantOut = null;
		// config.js
		Writer configJsOut = null;
		// pageUtil
		Writer pageUtilOut = null;
		// swaggerConfigOut
		Writer swaggerConfigOut = null;
		try {
			Template pomTemplate = getTemplate(CodeFtlConstant.pom_ftl);
			Template classPathTemplate = getTemplate(CodeFtlConstant.classpath_ftl);
			Template projectTemplate = getTemplate(CodeFtlConstant.project_ftl);
			Template castUtilTemplate = getTemplate(CodeFtlConstant.castutil_ftl);
			Template homehtmlTemplate = getTemplate(CodeFtlConstant.home_ftl);
			Template welcomehtmlTemplate = getTemplate(CodeFtlConstant.welcome_ftl);
			Template IBaseDaoTemplate = getTemplate(CodeFtlConstant.IBaseDao_ftl);
			Template excelUtilTemplate = getTemplate(CodeFtlConstant.excelUtil_ftl);
			Template logbackTemplate = getTemplate(CodeFtlConstant.logback_ftl);
			Template mybatisConfigTemplate = getTemplate(CodeFtlConstant.mybatisConfig_ftl);
			Template userControllerTemplate = getTemplate(CodeFtlConstant.userController_ftl);
			Template loginHtmlTemplate = getTemplate(CodeFtlConstant.loginHtml_ftl);
			Template userTemplate = getTemplate(CodeFtlConstant.user_ftl);
			Template allExceptionHandlerTemplate = getTemplate(CodeFtlConstant.allExceptionHandler_ftl);
			Template logAopAspectTemplate = getTemplate(CodeFtlConstant.logAopAspect_ftl);
			Template loggerUtilTemplate = getTemplate(CodeFtlConstant.loggerUtil_ftl);
			// 判断当前参数配置是否为JavaBean
			Template commonEntityTemplate = getTemplate(CodeFtlConstant.commonEntity_ftl);
			Template pageDataTemplate = getTemplate(CodeFtlConstant.pageData_ftl);
			// =============================================JavaBean==========================================
			// ===========================公共返回类 和 config.js===========================
			Template commonResultTemplate = getTemplate(CodeFtlConstant.commonResult_ftl);
			Template resultConstantTemplate = getTemplate(CodeFtlConstant.resultConstant_ftl);
			Template configJsTemplate = getTemplate(CodeFtlConstant.configJs_ftl);
			// ===========================公共返回类 和 config.js===========================
			// pageUtil
			Template pageUtilTemplate = getTemplate(CodeFtlConstant.pageUtil_ftl);
			Template swaggerConfigTemplate = null;
			boolean ifSwagger = "是".equals(ChildWindowConstant.commonParametersModel.getIfUseSwagger());
			if (ifSwagger) {
				// swagger
				swaggerConfigTemplate = getTemplate(CodeFtlConstant.swaggerConfig_ftl);
			}
			// pom.xml的输出流
			pomOut = new OutputStreamWriter(
					new FileOutputStream(FreeOutUtil.checkAndMakeDir(outPathVal + projectDir) + "pom.xml"), StandardCharsets.UTF_8);
			// 生成pom.xml
			pomTemplate.process(root, pomOut);
			// .classpath的输出流
			clpassPathOut = new OutputStreamWriter(
					new FileOutputStream(FreeOutUtil.checkAndMakeDir(outPathVal + projectDir) + ".classpath"), StandardCharsets.UTF_8);
			// 生成.classpath
			classPathTemplate.process(root, clpassPathOut);
			// .project的输出流
			projectOut = new OutputStreamWriter(
					new FileOutputStream(FreeOutUtil.checkAndMakeDir(outPathVal + projectDir) + ".project"), StandardCharsets.UTF_8);
			// 生成.project
			projectTemplate.process(root, projectOut);
			// castUtil的输出流
			castUtilOut = new OutputStreamWriter(
					new FileOutputStream(FreeOutUtil.checkAndMakeDir(outPathVal + utilDir) + "CastUtil.java"), StandardCharsets.UTF_8);
			// 生成 castUtil
			castUtilTemplate.process(root, castUtilOut);
			// homehtml的输出流
			if (CodeConstant.oldTheme.equals(themeVal)) {
				homeHtmlOut = new OutputStreamWriter(
						new FileOutputStream(FreeOutUtil.checkAndMakeDir(outPathVal + htmlDir) + "home.html"), StandardCharsets.UTF_8);
			} else {
				homeHtmlOut = new OutputStreamWriter(new FileOutputStream(webClientHtmlPath + "home.html"), StandardCharsets.UTF_8);
			}
			// 生成 homehtml
			homehtmlTemplate.process(root, homeHtmlOut);
			// welcomehtml的输出流
			if (CodeConstant.oldTheme.equals(themeVal)) {
				welcomeHtmlOut = new OutputStreamWriter(
						new FileOutputStream(FreeOutUtil.checkAndMakeDir(outPathVal + htmlDir) + "welcome.html"),
						StandardCharsets.UTF_8);
			} else {
				welcomeHtmlOut = new OutputStreamWriter(new FileOutputStream(webClientHtmlPath + "welcome.html"),
						StandardCharsets.UTF_8);
			}
			// 生成 welcomehtml
			welcomehtmlTemplate.process(root, welcomeHtmlOut);
			// IBaseDao的输出流
			IBaseDaoOut = new OutputStreamWriter(
					new FileOutputStream(FreeOutUtil.checkAndMakeDir(outPathVal + daoDir) + "IBaseDao.java"), StandardCharsets.UTF_8);
			// 生成 IBaseDao
			IBaseDaoTemplate.process(root, IBaseDaoOut);
			// excelUtil的输出流
			excelUtilOut = new OutputStreamWriter(
					new FileOutputStream(FreeOutUtil.checkAndMakeDir(outPathVal + utilDir) + "ExcelUtil.java"),
					StandardCharsets.UTF_8);
			// 生成 IBaseDao
			excelUtilTemplate.process(root, excelUtilOut);
			// logback的输出流
			logbackOut = new OutputStreamWriter(
					new FileOutputStream(FreeOutUtil.checkAndMakeDir(outPathVal + resourcesDir) + "logback.xml"),
					StandardCharsets.UTF_8);
			// 生成 logback
			logbackTemplate.process(root, logbackOut);
			// mybatis-config的输出流
			mybatisConfigOut = new OutputStreamWriter(
					new FileOutputStream(FreeOutUtil.checkAndMakeDir(outPathVal + sqlMapperDir) + "mybatis-config.xml"),
					StandardCharsets.UTF_8);
			// 生成 mybatis-config
			mybatisConfigTemplate.process(root, mybatisConfigOut);
			// loginController的输出流
			userControllerOut = new OutputStreamWriter(new FileOutputStream(
					FreeOutUtil.checkAndMakeDir(outPathVal + controllerDir) + "LoginController.java"), StandardCharsets.UTF_8);
			// 生成 loginController
			userControllerTemplate.process(root, userControllerOut);
			// loginHtml的输出流
			if (CodeConstant.oldTheme.equals(themeVal)) {
				loginHtmlOut = new OutputStreamWriter(
						new FileOutputStream(FreeOutUtil.checkAndMakeDir(outPathVal + htmlDir) + "login.html"),
						StandardCharsets.UTF_8);
			} else {
				loginHtmlOut = new OutputStreamWriter(new FileOutputStream(webClientHtmlPath + "login.html"), StandardCharsets.UTF_8);
			}
			// 生成 loginHtml
			loginHtmlTemplate.process(root, loginHtmlOut);
			// user的输出流
			userModelOut = new OutputStreamWriter(
					new FileOutputStream(FreeOutUtil.checkAndMakeDir(outPathVal + entityDir) + "User.java"), StandardCharsets.UTF_8);
			// 生成 user
			userTemplate.process(root, userModelOut);
			// allExceptionHandler的输出流
			allExceptionHandlerOut = new OutputStreamWriter(
					new FileOutputStream(
							FreeOutUtil.checkAndMakeDir(outPathVal + mvcConfigDir) + "AllExceptionHandler.java"),
					StandardCharsets.UTF_8);
			// 生成 allExceptionHandler
			allExceptionHandlerTemplate.process(root, allExceptionHandlerOut);
			// logAopAspect的输出流
			logAopAspectOut = new OutputStreamWriter(
					new FileOutputStream(FreeOutUtil.checkAndMakeDir(outPathVal + mvcConfigDir) + "LogAopAspect.java"),
					StandardCharsets.UTF_8);
			// 生成 logAopAspect
			logAopAspectTemplate.process(root, logAopAspectOut);
			// loggerUtil的输出流
			loggerUtilOut = new OutputStreamWriter(
					new FileOutputStream(FreeOutUtil.checkAndMakeDir(outPathVal + utilDir) + "LoggerUtil.java"),
					StandardCharsets.UTF_8);
			// 生成 loggerUtil
			loggerUtilTemplate.process(root, loggerUtilOut);
			// 生成 commonResult
			commonResultOut = new OutputStreamWriter(
					new FileOutputStream(FreeOutUtil.checkAndMakeDir(outPathVal + entityDir) + "CommonResult.java"),
					StandardCharsets.UTF_8);
			// 生成 CommonResult
			commonResultTemplate.process(root, commonResultOut);
			// 生成 resultConstant
			resultConstantOut = new OutputStreamWriter(
					new FileOutputStream(FreeOutUtil.checkAndMakeDir(outPathVal + constantDir) + "ResultConstant.java"),
					StandardCharsets.UTF_8);
			// 生成 resultConstant
			resultConstantTemplate.process(root, resultConstantOut);
			// 生成 pageUtil
			pageUtilOut = new OutputStreamWriter(
					new FileOutputStream(FreeOutUtil.checkAndMakeDir(outPathVal + utilDir) + "PageUtil.java"), StandardCharsets.UTF_8);
			// 生成 pageUtil
			pageUtilTemplate.process(root, pageUtilOut);
			if (ifSwagger) {
				// 生成 swaggerConfig
				swaggerConfigOut = new OutputStreamWriter(
						new FileOutputStream(
								FreeOutUtil.checkAndMakeDir(outPathVal + mvcConfigDir) + "SwaggerConfig.java"),
						StandardCharsets.UTF_8);
				// 生成 swaggerConfig
				swaggerConfigTemplate.process(root, swaggerConfigOut);
			}
			// 根据主题的不同生成的路径不同
			if (CodeConstant.oldTheme.equals(themeVal)) {
				// 生成config.js
				configJsOut = new OutputStreamWriter(new FileOutputStream(
						FreeOutUtil.checkAndMakeDir(outPathVal + staticDir + "js/") + "config.js"), StandardCharsets.UTF_8);
				// 生成 config.js
				configJsTemplate.process(root, configJsOut);
			} else {
				// 生成config.js
				configJsOut = new OutputStreamWriter(new FileOutputStream(webClientJsConfigPath + "config.js"),
						StandardCharsets.UTF_8);
				// 生成 config.js
				configJsTemplate.process(root, configJsOut);
			}
			// commonEntity的输出流
			commonEntityOut = new OutputStreamWriter(
					new FileOutputStream(FreeOutUtil.checkAndMakeDir(outPathVal + entityDir) + "CommonEntity.java"),
					StandardCharsets.UTF_8);
			// 生成 commonEntity
			commonEntityTemplate.process(root, commonEntityOut);
			// pageData的输出流
			pageDataOut = new OutputStreamWriter(
					new FileOutputStream(FreeOutUtil.checkAndMakeDir(outPathVal + entityDir) + "PageData.java"),
					StandardCharsets.UTF_8);
			// 生成 commonEntity
			pageDataTemplate.process(root, pageDataOut);
			pomOut.flush();
			clpassPathOut.flush();
			projectOut.flush();
			castUtilOut.flush();
			homeHtmlOut.flush();
			welcomeHtmlOut.flush();
			IBaseDaoOut.flush();
			excelUtilOut.flush();
			logbackOut.flush();
			mybatisConfigOut.flush();
			userControllerOut.flush();
			loginHtmlOut.flush();
			userModelOut.flush();
			allExceptionHandlerOut.flush();
			logAopAspectOut.flush();
			loggerUtilOut.flush();
			commonResultOut.flush();
			resultConstantOut.flush();
			configJsOut.flush();
			commonEntityOut.flush();
			pageDataOut.flush();
			if (swaggerConfigOut != null) {
				swaggerConfigOut.flush();
			}
		} finally {
			if (pomOut != null) {
				pomOut.close();
			}
			if (clpassPathOut != null) {
				clpassPathOut.close();
			}
			if (projectOut != null) {
				projectOut.close();
			}
			if (castUtilOut != null) {
				castUtilOut.close();
			}
			if (homeHtmlOut != null) {
				homeHtmlOut.close();
			}
			if (welcomeHtmlOut != null) {
				welcomeHtmlOut.close();
			}
			if (IBaseDaoOut != null) {
				IBaseDaoOut.close();
			}
			if (excelUtilOut != null) {
				excelUtilOut.close();
			}
			if (logbackOut != null) {
				logbackOut.close();
			}
			if (mybatisConfigOut != null) {
				mybatisConfigOut.close();
			}
			if (userControllerOut != null) {
				userControllerOut.close();
			}
			if (loginHtmlOut != null) {
				loginHtmlOut.close();
			}
			if (userModelOut != null) {
				userModelOut.close();
			}
			if (allExceptionHandlerOut != null) {
				allExceptionHandlerOut.close();
			}
			if (logAopAspectOut != null) {
				logAopAspectOut.close();
			}
			if (loggerUtilOut != null) {
				loggerUtilOut.close();
			}
			if (commonResultOut != null) {
				commonResultOut.close();
			}
			if (resultConstantOut != null) {
				resultConstantOut.close();
			}
			if (configJsOut != null) {
				configJsOut.close();
			}
			if (commonEntityOut != null) {
				commonEntityOut.close();
			}
			if (pageDataOut != null) {
				pageDataOut.close();
			}
			if (swaggerConfigOut != null) {
				swaggerConfigOut.close();
			}
		}
	}

	/**
	 * 单表相关代码生成
	 *
	 * @throws Exception
	 */
	public static boolean getSingleTableCode(String outPathVal, String ifJavaBean, String entityDir,
											 String daoDir, String IServiceDir, String serviceDir, String controllerDir,
											 String sqlMapperDir, String themeVal,
											 String htmlDir, String webClientHtmlPath,
											 String[] tableNameArr, String dataBaseTypeVal, Map<String, Object> root) throws Exception {
		Template entityTemplate = null;
		// 实体类的模板
		if ("JavaBean".equals(ifJavaBean)) {
			entityTemplate = CodeWriterUtil.getTemplate(CodeFtlConstant.entity_ftl);
		}
		Template daoTemplate = getTemplate(CodeFtlConstant.dao_ftl);
		Template IServiceTemplate = getTemplate(CodeFtlConstant.Iservice_ftl);
		Template serviceTemplate = getTemplate(CodeFtlConstant.service_ftl);
		Template controllerTemplate = getTemplate(CodeFtlConstant.controller_ftl);
		Template sqlMapperTemplate = getTemplate(CodeFtlConstant.sqlMapper_ftl);
		Template htmlTemplate = getTemplate(CodeFtlConstant.html_ftl);
		for (String currentTableName : tableNameArr) {
			root.put(CodeConstant.realTableName, currentTableName);
			String captureurrentTableName = CodeWriterUtil.captureName(currentTableName);
			// 如果是JavaBean配置
			if ("JavaBean".equals(ifJavaBean)) {
				// bean的名称
				root.put(CodeConstant.entityName, captureurrentTableName + "Entity");
				List<TableNameAndType> tableNameAndTypes = ChildWindowConstant.currentTableNameAndTypes.get(currentTableName);
				root.put(CodeConstant.entityNameAndTypes,
						tableNameAndTypes);
			}
			root.put(CodeConstant.IDaoName, "I" + captureurrentTableName + "Dao");
			root.put(CodeConstant.IServiceName, "I" + captureurrentTableName + "Service");
			root.put(CodeConstant.serviceName, captureurrentTableName + "Service");
			root.put(CodeConstant.controllerName, captureurrentTableName + "Controller");
			root.put(CodeConstant.controllerPrefix,
					captureurrentTableName.length() > 1
							? captureurrentTableName.substring(0, 1).toLowerCase()
							+ captureurrentTableName.substring(1)
							: captureurrentTableName.substring(0, 1).toLowerCase());
			root.put(CodeConstant.tableName, captureurrentTableName);
			List<String> primaryKeyList = ChildWindowConstant.primaryKeyListMap.get(currentTableName);
			String currentTableCnName = ChildWindowConstant.currentTableCnNameMap.get(currentTableName);
			//root.put(CodeConstant.primaryKeyList, primaryKeyList);
			root.put(CodeConstant.currentTableCnName, currentTableCnName);
			// 因为主键必须设置 所以所有的条件不可能为0
			List<DatabaseModel> allColumnList = ChildWindowConstant.allColumnMsgMap.get(currentTableName);
			List<DatabaseModel> sortAllColumnList = new ArrayList<>(allColumnList);
			if ("mysql".equals(dataBaseTypeVal)) {
				sortAllColumnList.sort((o1, o2) -> {
					if (o1.getWhereNo().compareTo(o2.getWhereNo()) == 0) {
						return 0;
					} else if (o2.getWhereNo().compareTo(o1.getWhereNo()) == 1) {
						return -1;
					} else {
						return 1;
					}
				});
			} else if ("oracle".equals(dataBaseTypeVal)) {
				sortAllColumnList.sort((o1, o2) -> {
					if (o1.getWhereNo().compareTo(o2.getWhereNo()) == 0) {
						return 0;
					} else if (o2.getWhereNo().compareTo(o1.getWhereNo()) == 1) {
						return 1;
					} else {
						return -1;
					}
				});
			}
			// sql条件排序使用
			root.put(CodeConstant.columnList, sortAllColumnList);
			// 页面显示使用
			List<DatabaseModel> selectColumnList = ChildWindowConstant.columnMsgMap.get(currentTableName);
			selectColumnList.sort((o1, o2) -> {
				if (o1.getShowNo().compareTo(o2.getShowNo()) == 0) {
					return 0;
				} else if (o2.getShowNo().compareTo(o1.getShowNo()) == 1) {
					return -1;
				} else {
					return 1;
				}
			});
			// 如果查询的个数为0 则默认查全部
			if (selectColumnList.size() == 0) {
				selectColumnList.addAll(allColumnList);
				root.put(CodeConstant.selectColumnList, allColumnList);
			} else {
				// 设置的查询字段列表
				root.put(CodeConstant.selectColumnList, selectColumnList);
			}
			List<DatabaseModel> updateColumnList = ChildWindowConstant.updateColumnMsgMap.get(currentTableName);
			// 如果没有配置更新，以初始展示的项目为准
			if (updateColumnList.size() == 0) {
				updateColumnList.addAll(selectColumnList);
				root.put(CodeConstant.updateColumnList, selectColumnList);
			} else {
				root.put(CodeConstant.updateColumnList, updateColumnList);
			}
			List<DatabaseModel> queryColumnList = ChildWindowConstant.queryColumnMsgMap.get(currentTableName);
			// 如果没有配置更新，以初始展示的项目为准
			if (queryColumnList.size() == 0) {
				root.put(CodeConstant.queryColumnList, selectColumnList);
			} else {
				root.put(CodeConstant.queryColumnList, queryColumnList);
			}
			List<DatabaseModel> primaryKeyModelList = new ArrayList<>();
			for (String primaryKey : primaryKeyList) {
				DatabaseModel model = new DatabaseModel();
				model.setColumnsEng(primaryKey);
				//设置sqlParamColumnEng
				model.setSqlParamColumnEng(DataUtils.getSqlParam(primaryKey));
				primaryKeyModelList.add(model);
			}
			// 遍历主键的模型list，判断在selectColumnList中存不存在,如不存在，加入
			List<DatabaseModel> selectColumnListMapper = new ArrayList<>(selectColumnList);
			for (DatabaseModel model : primaryKeyModelList) {
				// 如果没有，添加
				if (!selectColumnListMapper.contains(model)) {
					selectColumnListMapper.add(model);
				}
				/*
				 * if (updateColumnList.contains(model)) { updateColumnList.remove(model); }
				 */
			}
			root.put(CodeConstant.primaryKeyModelList, primaryKeyModelList);
			root.put(CodeConstant.selectColumnListMapper, selectColumnListMapper);
			// 实体类输出
			Writer entityOut = null;
			Writer daoOut = null;
			Writer IServiceOut = null;
			Writer serviceOut = null;
			Writer controllerOut = null;
			Writer sqlMapperOut = null;
			Writer htmlOut = null;
			try {
				if ("JavaBean".equals(ifJavaBean)) {
					// dao接口的输出文件
					entityOut = new OutputStreamWriter(
							new FileOutputStream(FreeOutUtil.checkAndMakeDir(outPathVal + entityDir)
									+ captureurrentTableName + "Entity.java"),
							StandardCharsets.UTF_8);
				}
				// dao接口的输出文件
				daoOut = new OutputStreamWriter(
						new FileOutputStream(FreeOutUtil.checkAndMakeDir(outPathVal + daoDir) + "I"
								+ captureurrentTableName + "Dao.java"),
						StandardCharsets.UTF_8);
				// service接口的输出文件
				IServiceOut = new OutputStreamWriter(
						new FileOutputStream(FreeOutUtil.checkAndMakeDir(outPathVal + IServiceDir) + "I"
								+ captureurrentTableName + "Service.java"),
						StandardCharsets.UTF_8);
				// service实现类的输出文件
				serviceOut = new OutputStreamWriter(
						new FileOutputStream(FreeOutUtil.checkAndMakeDir(outPathVal + serviceDir)
								+ captureurrentTableName + "Service.java"),
						StandardCharsets.UTF_8);
				// controller的输出文件
				controllerOut = new OutputStreamWriter(
						new FileOutputStream(FreeOutUtil.checkAndMakeDir(outPathVal + controllerDir)
								+ captureurrentTableName + "Controller.java"),
						StandardCharsets.UTF_8);
				// sqlMapper.xml的输出文件
				sqlMapperOut = new OutputStreamWriter(new FileOutputStream(
						FreeOutUtil.checkAndMakeDir(outPathVal + sqlMapperDir) + currentTableName + "Mapper.xml"),
						StandardCharsets.UTF_8);
				// 当前表html的输出文件
				if (CodeConstant.oldTheme.equals(themeVal)) {
					htmlOut = new OutputStreamWriter(new FileOutputStream(
							FreeOutUtil.checkAndMakeDir(outPathVal + htmlDir + currentTableName + "/")
									+ "list.html"),
							StandardCharsets.UTF_8);
					// 前后端分离主题
				} else {
					htmlOut = new OutputStreamWriter(new FileOutputStream(
							FreeOutUtil.checkAndMakeDir(webClientHtmlPath + currentTableName + "/") + "list.html"),
							StandardCharsets.UTF_8);
				}
				// 生成每个分层的代码
				if ("JavaBean".equals(ifJavaBean)) {
					entityTemplate.process(root, entityOut);
				}
				daoTemplate.process(root, daoOut);
				IServiceTemplate.process(root, IServiceOut);
				serviceTemplate.process(root, serviceOut);
				controllerTemplate.process(root, controllerOut);
				sqlMapperTemplate.process(root, sqlMapperOut);
				htmlTemplate.process(root, htmlOut);
				if ("JavaBean".equals(ifJavaBean)) {
					entityOut.flush();
				}
				daoOut.flush();
				IServiceOut.flush();
				serviceOut.flush();
				controllerOut.flush();
				sqlMapperOut.flush();
				htmlOut.flush();
			} catch (IOException e) {
				JOptionPane.showMessageDialog(Constant.frmv, "表 " + currentTableName + " 生成代码时出错！", "错误",
						JOptionPane.ERROR_MESSAGE);
				return false;
			} catch (TemplateException e) {
				JOptionPane.showMessageDialog(Constant.frmv, "解析模板文件时出错！", "错误", JOptionPane.ERROR_MESSAGE);
				return false;
				// 关闭所有的文件流
			} finally {
				if (entityOut != null) {
					entityOut.close();
				}
				if (daoOut != null) {
					daoOut.close();
				}
				if (IServiceOut != null) {
					IServiceOut.close();
				}
				if (serviceOut != null) {
					serviceOut.close();
				}
				if (controllerOut != null) {
					controllerOut.close();
				}

				if (sqlMapperOut != null) {
					sqlMapperOut.close();
				}
				if (htmlOut != null) {
					htmlOut.close();
				}
			}
		}
		return true;
	}

	/**
	 * 多表相关代码生成
	 *
	 * @throws Exception
	 */
	public static boolean getMutiTableCode(String outPathVal, String sqlMapperDir,
										   String IServiceDir, String serviceDir,
										   String daoDir, String controllerDir,
										   String themeVal, String entityDir, String htmlDir,
										   String webClientHtmlPath, Map<String, Object> root) throws Exception {
		Template mutiTableSqlMapperTemplate = getTemplate(CodeFtlConstant.MutiTableSqlMapper_ftl);
		Template IMutiTableDaoTemplate = getTemplate(CodeFtlConstant.IMutiTableDao_ftl);
		Template IMutiTableServiceTemplate = getTemplate(CodeFtlConstant.IMutiTableService_ftl);
		Template mutiTableServiceTemplate = getTemplate(CodeFtlConstant.MutiTableService_ftl);
		Template mutiTableControllerTemplate = getTemplate(CodeFtlConstant.MutiTableController_ftl);
		Template mutiTableHtmlTemplate = getTemplate(CodeFtlConstant.mutiTableHtml_ftl);
		Template mutiTableEntityTemplate = getTemplate(CodeFtlConstant.mutiTableEntity_ftl);
		// 多表配置生成文件
		for (Map.Entry<String, Map<String, TablesQueryModel>> methodEntry : ChildWindowConstant.tablesQueryMap
				.entrySet()) {
			// 当前模块英文名
			String currentMutiEng = methodEntry.getKey();
			// 当前模块中文名
			String currentMutiCn = ChildWindowConstant.tablesQueryEndAndCnMap.get(currentMutiEng);
			root.put(CodeConstant.currentMutiEng, currentMutiEng);
			String capCurrentMutiEng = CodeWriterUtil.captureName(currentMutiEng);
			root.put(CodeConstant.capCurrentMutiEng, capCurrentMutiEng);
			root.put(CodeConstant.currentMutiCn, currentMutiCn);
			Map<String, TablesQueryModel> methodMap = methodEntry.getValue();
			// 方法的map
			root.put(CodeConstant.currentMethodMap, methodMap);
			Writer mutiTableSqlMapperOut = null;
			Writer IMutiTableDaoOut = null;
			Writer IMutiTableServiceOut = null;
			Writer mutiTableServiceOut = null;
			Writer mutiTableControllerOut = null;
			try {
				mutiTableSqlMapperOut = new OutputStreamWriter(
						new FileOutputStream(FreeOutUtil.checkAndMakeDir(outPathVal + sqlMapperDir)
								+ capCurrentMutiEng + "MutiSqlMapper.xml"),
						StandardCharsets.UTF_8);
				mutiTableSqlMapperTemplate.process(root, mutiTableSqlMapperOut);
				IMutiTableDaoOut = new OutputStreamWriter(
						new FileOutputStream(FreeOutUtil.checkAndMakeDir(outPathVal + daoDir) + "I"
								+ capCurrentMutiEng + "MutiDao.java"),
						StandardCharsets.UTF_8);
				IMutiTableDaoTemplate.process(root, IMutiTableDaoOut);
				IMutiTableServiceOut = new OutputStreamWriter(
						new FileOutputStream(FreeOutUtil.checkAndMakeDir(outPathVal + IServiceDir) + "I"
								+ capCurrentMutiEng + "MutiService.java"),
						StandardCharsets.UTF_8);
				IMutiTableServiceTemplate.process(root, IMutiTableServiceOut);
				mutiTableServiceOut = new OutputStreamWriter(
						new FileOutputStream(FreeOutUtil.checkAndMakeDir(outPathVal + serviceDir)
								+ capCurrentMutiEng + "MutiService.java"),
						StandardCharsets.UTF_8);
				mutiTableServiceTemplate.process(root, mutiTableServiceOut);
				mutiTableControllerOut = new OutputStreamWriter(
						new FileOutputStream(FreeOutUtil.checkAndMakeDir(outPathVal + controllerDir)
								+ capCurrentMutiEng + "MutiController.java"),
						StandardCharsets.UTF_8);
				mutiTableControllerTemplate.process(root, mutiTableControllerOut);
				mutiTableControllerOut.flush();
				mutiTableServiceOut.flush();
				IMutiTableServiceOut.flush();
				IMutiTableDaoOut.flush();
				mutiTableSqlMapperOut.flush();
			} catch (Exception e1) {
				JOptionPane.showMessageDialog(Constant.frmv, "多表配置生成公共类时解析模板文件时出错！", "错误", JOptionPane.ERROR_MESSAGE);
				return false;
			} finally {
				if (mutiTableControllerOut != null) {
					mutiTableControllerOut.close();
				}
				if (mutiTableServiceOut != null) {
					mutiTableServiceOut.close();
				}
				if (IMutiTableServiceOut != null) {
					IMutiTableServiceOut.close();
				}
				if (IMutiTableDaoOut != null) {
					IMutiTableDaoOut.close();
				}
				if (mutiTableSqlMapperOut != null) {
					mutiTableSqlMapperOut.close();
				}
			}
			// 遍历当前模块的所有方法 一个方法对应一个entity和一个html
			for (Map.Entry<String, TablesQueryModel> method : methodMap.entrySet()) {
				// 当前方法名
				String methodName = method.getKey();
				TablesQueryModel tablesQueryModel = method.getValue();
				// 当前方法中文名
				String methodNameCn = tablesQueryModel.getMethodName_cn();
				// 当前方法实体名 首字母大写
				String entityName = CodeWriterUtil.captureName(tablesQueryModel.getEntityName());
				// 当前方法实体中文名
				String entityName_cn = tablesQueryModel.getEntityName_cn();
				List<TableFiledModel> tableFiledModels = tablesQueryModel.getTableFiledModels();
				List<TableConditionModel> tableConditionModels = tablesQueryModel.getTableConditionModels();
				// entity的集合
				List<EntityFieldModel> entityFieldList = new ArrayList<>();
				for (TableFiledModel model : tableFiledModels) {
					EntityFieldModel entityFieldModel = new EntityFieldModel();
					entityFieldModel.setFieldName(model.getAnotherFiledName());
					entityFieldModel.setFieldName_cn(model.getFiledText_cn());
					entityFieldModel.setFieldType(model.getFiledType());
					entityFieldModel.setCompareText("");
					entityFieldList.add(entityFieldModel);
				}
				for (TableConditionModel model : tableConditionModels) {
					EntityFieldModel entityFieldModel = new EntityFieldModel();
					entityFieldModel.setFieldName(model.getAnotherTableName() + "_" + model.getFiled_eng());
					entityFieldModel.setFieldName_cn(model.getFiled_cn());
					entityFieldModel.setFieldType(model.getServiceType());
					entityFieldModel.setCompareText(model.getCompareText());
					if (!entityFieldList.contains(entityFieldModel) && "".equals(model.getUnchangeValue())) {
						entityFieldList.add(entityFieldModel);
					}
				}
				root.put(CodeConstant.currentMutiMethodEng, methodName);
				root.put(CodeConstant.currentMutiMethodCn, methodNameCn);
				root.put(CodeConstant.currentMutiEntityName, entityName + "Muti");
				root.put(CodeConstant.currentMutiEntityNameCn, entityName_cn);
				root.put(CodeConstant.tableFiledModels, tableFiledModels);
				root.put(CodeConstant.tableConditionModels, tableConditionModels);
				root.put(CodeConstant.entityFiledModels, entityFieldList);
				Writer mutiTableHtmlOut = null;
				Writer mutiEntityOut = null;
				try {
					// 一个方法一个html
					if (CodeConstant.oldTheme.equals(themeVal)) {
						mutiTableHtmlOut = new OutputStreamWriter(new FileOutputStream(
								FreeOutUtil.checkAndMakeDir(outPathVal + htmlDir + currentMutiEng + "Muti/")
										+ methodName + "List.html"),
								StandardCharsets.UTF_8);
					} else {
						mutiTableHtmlOut = new OutputStreamWriter(new FileOutputStream(
								FreeOutUtil.checkAndMakeDir(webClientHtmlPath + currentMutiEng + "Muti/")
										+ methodName + "List.html"),
								StandardCharsets.UTF_8);
					}
					mutiTableHtmlTemplate.process(root, mutiTableHtmlOut);
					mutiEntityOut = new OutputStreamWriter(new FileOutputStream(
							FreeOutUtil.checkAndMakeDir(outPathVal + entityDir) + entityName + "Muti" + ".java"),
							StandardCharsets.UTF_8);
					mutiTableEntityTemplate.process(root, mutiEntityOut);
					mutiTableHtmlOut.flush();
					mutiEntityOut.flush();
				} catch (Exception e) {
					JOptionPane.showMessageDialog(Constant.frmv, "多表配置生成每个方法时解析模板文件时出错！", "错误", JOptionPane.ERROR_MESSAGE);
					return false;
				} finally {
					if (mutiEntityOut != null) {
						mutiEntityOut.close();
					}
					if (mutiTableHtmlOut != null) {
						mutiTableHtmlOut.close();
					}
				}
			}
		}
		return true;
	}

	/**
	 * 生成setting文件夹下的代码（eclipse用户使用）
	 */
	public static boolean getSettingCode(String outPathVal, String frameWorkVal, String settingsDir) {
		try {
			String realSettingDir = FreeOutUtil.checkAndMakeDir(outPathVal + settingsDir);
			File mvcSettingsZip = new File(Constant.mvcSettingsZip);
			File settingsZip = new File(Constant.settingsZip);
			if ("ssm".equals(frameWorkVal)) {
				if (!mvcSettingsZip.exists()) {
					mvcSettingsZip = HttpDownloadUtil.downloadFile(Constant.downMvcSettingsZipNet,
							Constant.modelFiles);
				}
				ZipUtils.unZip(mvcSettingsZip, realSettingDir, Constant.zipPwd);
			}
			if ("springBoot".equals(frameWorkVal)) {
				if (!settingsZip.exists()) {
					settingsZip = HttpDownloadUtil.downloadFile(Constant.downSettingsZipNet, Constant.modelFiles);
				}
				ZipUtils.unZip(settingsZip, realSettingDir, Constant.zipPwd);
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(Constant.frmv, "生成配置文件setting时出错！可能是网络问题导致的，请重试！", "错误",
					JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}

	/**
	 * 生成static文件夹下的代码
	 */
	public static boolean getStaticCode(String outPathVal, String themeVal, String staticDir, String webClientPath) {
		if (CodeConstant.oldTheme.equals(themeVal)) {
			try {
				String realStaticDir = FreeOutUtil.checkAndMakeDir(outPathVal + staticDir);
				File staticZip = new File(Constant.staticZip);
				if (!staticZip.exists()) {
					staticZip = HttpDownloadUtil.downloadFile(Constant.downStaticZipNet, Constant.modelFiles);
				}
				ZipUtils.unZip(staticZip, realStaticDir, Constant.zipPwd);
			} catch (Exception e) {
				JOptionPane.showMessageDialog(Constant.frmv, "生成静态资源文件时出错！可能是网络问题导致的，请重试！", "错误",
						JOptionPane.ERROR_MESSAGE);
				return false;
			}
			// 如果是前后端分离响应式项目
		} else if (CodeConstant.hAdminTheme.equals(themeVal)) {
			try {
				File webClientZip = new File(Constant.webClientZip);
				if (!webClientZip.exists()) {
					webClientZip = HttpDownloadUtil.downloadFile(Constant.downWebClientZipNet, Constant.modelFiles);
				}
				ZipUtils.unZip(webClientZip, webClientPath, Constant.zipPwd);
			} catch (Exception e) {
				JOptionPane.showMessageDialog(Constant.frmv, "生成静态资源文件时出错！可能是网络问题导致的，请重试！", "错误",
						JOptionPane.ERROR_MESSAGE);
				return false;
			}
		}
		return true;
	}

	/**
	 * 生成ojdbc的lib文件夹
	 *
	 */
	public static boolean getOjdbcCode(String outPathVal, String frameWorkVal, String dataBaseTypeVal, String projectNameVal) {
		// 如果是oracle，需要添加ojdbc支持
		if ("oracle".equals(dataBaseTypeVal)) {
			// 下载lib.zip文件到项目路径下 oracle的驱动
			String libDir = null;
			if ("ssm".equals(frameWorkVal)) {
				libDir = FreeOutUtil.setProjectName(Constant.freeOutPath_lib_MVC, projectNameVal);
			}
			if ("springBoot".equals(frameWorkVal)) {
				libDir = FreeOutUtil.setProjectName(Constant.freeOutPath_lib, projectNameVal);
			}
			try {
				String realLibDir = FreeOutUtil.checkAndMakeDir(outPathVal + libDir);
				File libZip = new File(Constant.libZip);
				if (!libZip.exists()) {
					libZip = HttpDownloadUtil.downloadFile(Constant.downLibZipNet, Constant.modelFiles);
				}
				ZipUtils.unZip(libZip, realLibDir, Constant.zipPwd);
			} catch (Exception e) {
				JOptionPane.showMessageDialog(Constant.frmv, "生成lib文件夹时出错！可能是网络问题导致的，请重试！", "错误",
						JOptionPane.ERROR_MESSAGE);
				return false;
			}
		}
		return true;
	}
}
