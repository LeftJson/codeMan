package util;

public class DataUtils {


    /**
     * @param columnsEng
     */
    public static String getSqlParam(String columnsEng) {

        String sqlParamColumEng = "";

        if (columnsEng.contains(".")) {
            columnsEng = columnsEng.toLowerCase();
            String[] nameArr = columnsEng.split("\\.");
            sqlParamColumEng = makeSqlParam(nameArr);
        } else if (columnsEng.contains("_")) {
            columnsEng = columnsEng.toLowerCase();
            String[] nameArr = columnsEng.split("\\_");
            sqlParamColumEng = makeSqlParam(nameArr);
        } else if (columnsEng.contains("-")) {
            columnsEng = columnsEng.toLowerCase();
            String[] nameArr = columnsEng.split("-");
            sqlParamColumEng = makeSqlParam(nameArr);
        } else {
            if (columnsEng.matches("^[A-Z|0-9A-Z]+$")) {
                sqlParamColumEng = columnsEng.toLowerCase();
            } else {
                if (columnsEng.length() > 2) {
                    sqlParamColumEng = columnsEng.substring(0, 2).toLowerCase() + columnsEng.substring(2);
                } else {
                    sqlParamColumEng = columnsEng.toLowerCase();
                }

            }
        }

        return sqlParamColumEng;
    }

    private static String makeSqlParam(String[] nameArr) {

        String sqlParamColumEng = "";
        for (int j = 0; j < nameArr.length; j++) {
            if (j == 0) {
                sqlParamColumEng += nameArr[j];
            } else if (nameArr[j].length() > 1) {
                sqlParamColumEng += nameArr[j].substring(0, 1).toUpperCase() + nameArr[j].substring(1);
            } else {
                sqlParamColumEng += nameArr[j].toUpperCase();
            }
        }
        if (sqlParamColumEng.length() > 2) {
            sqlParamColumEng = sqlParamColumEng.substring(0, 2).toLowerCase() + sqlParamColumEng.substring(2);
        } else {
            sqlParamColumEng = sqlParamColumEng.toLowerCase();
        }
        return sqlParamColumEng;
    }
}
