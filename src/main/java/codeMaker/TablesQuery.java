package codeMaker;

import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;

import constant.ChildWindowConstant;
import constant.CodeConstant;
import constant.CompareComboBox;
import constant.FiledComment;
import constant.Filed_cn;
import constant.Filed_eng;
import constant.OrderComboBox;
import constant.RelationJcombox;
import constant.ServiceTypeComboBox;
import constant.Table1;
import constant.Table1Fileds;
import constant.Table2;
import constant.Table2Fileds;
import entity.Parameters;
import entity.TableConditionModel;
import entity.TableFiledModel;
import entity.TableRelationModel;
import entity.TablesQueryModel;

public class TablesQuery {

	private static String currentDataBaseName;
	private static String currentDataBaseUrl;
	private JFrame frame;
	private JTextField currentModelName;
	private JTextField currentModelName_cn;
	private JTextField methodNameField;
	private JTextField methodName_cnField;
	private JTextField entityNameField;
	private JTextField entityName_cnField;

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	/**
	 * Launch the application.
	 */
	public static void main(final Parameters parameters) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					if (currentDataBaseName == null || currentDataBaseUrl == null) {
						// 记录当前数据库的名称和url
						currentDataBaseName = parameters.getDataBaseNameVal();
						currentDataBaseUrl = parameters.getDataBaseUrl();
					} else {
						// 如果数据库不一致，清空map
						if (!currentDataBaseName.equals(parameters.getDataBaseNameVal())
								|| !currentDataBaseUrl.equals(parameters.getDataBaseUrl())) {
							ChildWindowConstant.tablesQueryMap.clear();
							ChildWindowConstant.tablesQueryEndAndCnMap.clear();
							// 记录当前数据库的名称和url
							currentDataBaseName = parameters.getDataBaseNameVal();
							currentDataBaseUrl = parameters.getDataBaseUrl();
						}

					}

					TablesQuery window = new TablesQuery(parameters);
					ChildWindowConstant.tablesQuery = window;
					window.frame.setVisible(true);

					JOptionPane.showMessageDialog(window.frame,
							"表1字段、表2字段：多个字段使用 & 隔开，字段个数保持一致" + CodeConstant.NEW_LINE + "类型描述：" + CodeConstant.NEW_LINE
									+ "    布尔示例：是#否 其中是和否填写数据库中用来表示是和否的值" + CodeConstant.NEW_LINE
									+ "    状态码示例：状态名称&状态值#状态名称&状态值（填写状态名称和状态值便于前台生成样式）" + CodeConstant.NEW_LINE
									+ "    其他类型将根据类型生成对应的样式，无需描述" + CodeConstant.NEW_LINE + "注意：关联表信息配置完点击确定才会生效！！！",
							"提示", JOptionPane.INFORMATION_MESSAGE);
				} catch (Exception e) {

				}
			}
		});
	}

	/**
	 * test
	 *
	 * @param parameters
	 */
	/*
	 * public static void main(String[] args) { EventQueue.invokeLater(new
	 * Runnable() { public void run() { try {
	 *
	 * TablesQuery window = new TablesQuery(); ChildWindowConstant.tablesQuery =
	 * window; window.frame.setVisible(true);
	 *
	 * } catch (Exception e) {
	 *
	 * } } }); }
	 */

	/**
	 * Create the application.
	 */
	public TablesQuery(Parameters parameters) {
		initialize(parameters);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(Parameters parameters) {
		frame = new JFrame();
		frame.setResizable(true);
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage(TablesQuery.class
				.getResource("/org/pushingpixels/substance/internal/contrib/randelshofer/quaqua/images/palette.png")));
		frame.setTitle("多表查询配置");
		frame.setBounds(100, 100, 1186, 831);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		JScrollPane addTableScrollPane = new JScrollPane();

		JScrollPane queryScrollPane = new JScrollPane();

		JButton addTable = new JButton("添加关联表信息");

		JButton addQuery = new JButton("添加查询字段");

		JLabel label = new JLabel("当前模块英文名称");

		currentModelName = new JTextField();
		currentModelName.setColumns(10);

		JButton confirm = new JButton("确定");

		JLabel label_1 = new JLabel("查看已配置模块");

		JButton addCondition = new JButton("添加条件字段");

		addQuery.setEnabled(false);
		addCondition.setEnabled(false);

		JScrollPane conditionScrollPane = new JScrollPane();

		JButton restart = new JButton("清空重配");

		JComboBox<String> tableConfigExit = new JComboBox<String>();

		String[] modelArr = ChildWindowConstant.tablesQueryMap.keySet().toArray(new String[] {});

		tableConfigExit.setModel(new DefaultComboBoxModel<>(new String[] { "--请选择--" }));

		for (String model : modelArr) {
			tableConfigExit.addItem(model);
		}

		JButton confirmRelTable = new JButton("确定");
		JLabel label_3 = new JLabel("———》");

		JLabel cnLable = new JLabel("中文名称");

		currentModelName_cn = new JTextField();
		currentModelName_cn.setColumns(10);

		JButton button = new JButton("配置说明");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				JOptionPane.showMessageDialog(frame,
						"表1字段、表2字段：多个字段使用 & 隔开，字段个数保持一致" + CodeConstant.NEW_LINE + "实体名称：用于生成方法的参数和返回值"
								+ CodeConstant.NEW_LINE + "类型描述：" + CodeConstant.NEW_LINE
								+ "    布尔示例：是#否 其中是和否填写数据库中用来表示是和否的值" + CodeConstant.NEW_LINE
								+ "    状态码示例：状态名称&状态值#状态名称&状态值（填写状态名称和状态值便于前台生成样式）" + CodeConstant.NEW_LINE
								+ "    其他类型将根据类型生成对应的样式，无需描述" + CodeConstant.NEW_LINE + "注意：关联表信息配置完点击确定才会生效！！！",
						"提示", JOptionPane.INFORMATION_MESSAGE);
			}
		});

		JLabel label_2 = new JLabel("*");
		label_2.setForeground(Color.RED);

		JLabel methodNameLable = new JLabel("方法名");

		methodNameField = new JTextField();
		methodNameField.setColumns(10);

		JLabel lblNewLabel = new JLabel("*");
		lblNewLabel.setForeground(Color.RED);

		JLabel methodListLable = new JLabel("模块下方法");

		JComboBox<String> methodListCombox = new JComboBox<>();
		methodListCombox.setModel(new DefaultComboBoxModel<String>(new String[] { "--请选择--" }));

		JLabel label_5 = new JLabel("方法中文名称");

		methodName_cnField = new JTextField();
		methodName_cnField.setColumns(10);

		JLabel label_4 = new JLabel("实体名称");

		entityNameField = new JTextField();
		entityNameField.setColumns(10);

		JLabel label_6 = new JLabel("实体中文名称");

		entityName_cnField = new JTextField();
		entityName_cnField.setColumns(10);

		JLabel label_7 = new JLabel("*");
		label_7.setForeground(Color.RED);

		JButton deleteTable = new JButton("删除尾行");
		
		JButton deleteQuery = new JButton("删除尾行");
		
		JButton deleteCondition = new JButton("删除尾行");

		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(25)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(addQuery)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(deleteQuery))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(addTable)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(deleteTable))
						.addGroup(groupLayout.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
										.addGroup(groupLayout.createSequentialGroup()
											.addComponent(button)
											.addPreferredGap(ComponentPlacement.UNRELATED)
											.addComponent(restart)
											.addPreferredGap(ComponentPlacement.UNRELATED)
											.addComponent(confirm))
										.addGroup(groupLayout.createSequentialGroup()
											.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
												.addComponent(queryScrollPane, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 990, GroupLayout.PREFERRED_SIZE)
												.addComponent(addTableScrollPane, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 990, GroupLayout.PREFERRED_SIZE))
											.addPreferredGap(ComponentPlacement.RELATED)
											.addComponent(label_3)))
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(confirmRelTable, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(conditionScrollPane, GroupLayout.PREFERRED_SIZE, 1065, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED))
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(addCondition)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(deleteCondition))))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addComponent(cnLable)
								.addComponent(label))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
								.addComponent(currentModelName)
								.addComponent(currentModelName_cn, GroupLayout.DEFAULT_SIZE, 98, Short.MAX_VALUE))
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
								.addGroup(groupLayout.createSequentialGroup()
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(label_2)
									.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(methodNameLable))
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(35)
									.addComponent(label_5)))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(methodNameField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(lblNewLabel))
								.addComponent(methodName_cnField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(18)
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(label_4)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(entityNameField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(label_6)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(entityName_cnField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(label_7)
									.addGap(18)
									.addComponent(label_1))
								.addComponent(methodListLable))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(methodListCombox, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE)
								.addComponent(tableConfigExit, GroupLayout.PREFERRED_SIZE, 134, GroupLayout.PREFERRED_SIZE))))
					.addGap(42))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(label)
						.addComponent(currentModelName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_2)
						.addComponent(methodNameLable)
						.addComponent(methodNameField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNewLabel)
						.addComponent(entityNameField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_7)
						.addComponent(label_4)
						.addComponent(label_1)
						.addComponent(tableConfigExit, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(5)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(cnLable)
						.addComponent(currentModelName_cn, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_5)
						.addComponent(methodName_cnField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_6)
						.addComponent(entityName_cnField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(methodListLable)
						.addComponent(methodListCombox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(18)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(addTable)
								.addComponent(deleteTable))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(addTableScrollPane, GroupLayout.PREFERRED_SIZE, 138, GroupLayout.PREFERRED_SIZE)
							.addGap(22)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(addQuery)
								.addComponent(deleteQuery)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(143)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(label_3)
								.addComponent(confirmRelTable))))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(queryScrollPane, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE)
					.addGap(22)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(addCondition)
						.addComponent(deleteCondition))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(conditionScrollPane, GroupLayout.PREFERRED_SIZE, 139, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(button)
						.addComponent(restart)
						.addComponent(confirm))
					.addContainerGap())
		);

		JPanel conditionPanel = new JPanel();
		conditionScrollPane.setViewportView(conditionPanel);

		JPanel querysPanel = new JPanel();
		queryScrollPane.setViewportView(querysPanel);

		JPanel tablesPanel = new JPanel();
		addTableScrollPane.setViewportView(tablesPanel);

		tablesPanel.setLayout(new GridLayout(0, 5, 5, 5));
		querysPanel.setLayout(new GridLayout(0, 6, 5, 5));
		conditionPanel.setLayout(new GridLayout(0, 8, 5, 5));

		String[] tablesArr = new String[] { "关联关系", "表1", "表2", "表1字段", "表2字段" };
		String[] querysArr = new String[] { "表名", "字段", "中文名称", "字段类型", "类型描述", "是否可排序" };
		String[] conditionsArr = new String[] { "关联关系", "表名", "字段", "中文名称", "字段类型", "类型描述", "比较关系", "固定值" };

		List<TableRelationModel> tableRelationModelLists = new ArrayList<TableRelationModel>();
		Map<String, String> tableMap = new HashMap<>();

		addTable.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				addQuery.setEnabled(false);
				addCondition.setEnabled(false);

				if (tablesPanel.getComponentCount() == 0) {
					for (String tableStr : tablesArr) {
						JLabel label = new JLabel(tableStr);
						tablesPanel.add(label);
					}
					addTableMethod(tablesPanel);
					((Table2) tablesPanel.getComponent(7)).setEnabled(false);
					((Table1Fileds) tablesPanel.getComponent(8)).setEnabled(false);
					((Table1Fileds) tablesPanel.getComponent(8)).setText("/");
					((Table2Fileds) tablesPanel.getComponent(9)).setEnabled(false);
					((Table2Fileds) tablesPanel.getComponent(9)).setText("/");
				} else {
					addTableMethod(tablesPanel);
				}

				frame.validate();
				frame.repaint();

			}

			private void addTableMethod(JPanel tablesPanel) {

				List<String> table2List = new ArrayList<>();

				if (tablesPanel.getComponentCount() >= 5) {

					Component[] components = tablesPanel.getComponents();

					for (int i = 5; i < components.length; i++) {

						if ("Table1".equals(components[i].getClass().getSimpleName())) {

							Table1 table1 = (Table1) components[i];

							if ("".equals(table1.getText())) {
								JOptionPane.showMessageDialog(frame, "请先把当前内容填写完毕再增加关联表信息！", "提示",
										JOptionPane.INFORMATION_MESSAGE);
								return;
							}

							table2List.add(table1.getText());
						} else if ("Table1Fileds".equals(components[i].getClass().getSimpleName())) {

							Table1Fileds table1Fileds = (Table1Fileds) components[i];

							if ("".equals(table1Fileds.getText())) {
								JOptionPane.showMessageDialog(frame, "请先把当前内容填写完毕再增加关联表信息！", "提示",
										JOptionPane.INFORMATION_MESSAGE);
								return;
							}
						} else if ("Table2Fileds".equals(components[i].getClass().getSimpleName())) {

							Table2Fileds table2Fileds = (Table2Fileds) components[i];

							if ("".equals(table2Fileds.getText())) {
								JOptionPane.showMessageDialog(frame, "请先把当前内容填写完毕再增加关联表信息！", "提示",
										JOptionPane.INFORMATION_MESSAGE);
								return;
							}
						}

					}
				}

				JComboBox<String> relationComBox = null;
				// 如果是第一次添加
				if (tablesPanel.getComponentCount() == 5) {
					// 关联关系
					relationComBox = new JComboBox<>(new String[] { "from" });
				} else {
					// 关联关系
					relationComBox = new JComboBox<>(new String[] { "left join", "join", "right join" });
				}

				// 关联表
				Table1 table1 = new Table1();
				// 关联字段
				Table2 table2 = new Table2();

				if (tablesPanel.getComponentCount() >= 10) {
					table2.setModel(new DefaultComboBoxModel<>(table2List.toArray(new String[] {})));
				}

				Table1Fileds table1Fileds = new Table1Fileds();
				Table2Fileds table2Fileds = new Table2Fileds();

				tablesPanel.add(relationComBox);
				tablesPanel.add(table1);
				tablesPanel.add(table2);
				tablesPanel.add(table1Fileds);
				tablesPanel.add(table2Fileds);
			}
		});

		// 删除关联表尾行
		deleteTable.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				// 获取容器个数
				int componentCount = tablesPanel.getComponentCount();
				
				if (componentCount == 0) {
					return;
				}
				// 一行有五个
				// 如果是10，全部移除
				if (componentCount == 10) {
					tablesPanel.removeAll();
					// 否则，移除最后五个
				} else {
					tablesPanel.remove(componentCount - 1);
					tablesPanel.remove(componentCount - 2);
					tablesPanel.remove(componentCount - 3);
					tablesPanel.remove(componentCount - 4);
					tablesPanel.remove(componentCount - 5);
				}

				frame.validate();
				frame.repaint();

			}
		});

		addQuery.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (!canAddQuery(querysPanel)) {
					return;
				}

				if (querysPanel.getComponentCount() == 0) {
					for (String queryStr : querysArr) {
						JLabel label = new JLabel(queryStr);
						querysPanel.add(label);
					}
					addQueryMethod(querysPanel);
				} else {
					addQueryMethod(querysPanel);
				}

				frame.validate();
				frame.repaint();
			}

			private boolean canAddQuery(JPanel querysPanel) {

				if (querysPanel.getComponentCount() >= 10) {

					Component[] components = querysPanel.getComponents();
					for (int i = 6; i < components.length; i++) {

						if ("Filed_eng".equals(components[i].getClass().getSimpleName())) {

							Filed_eng filed_eng = (Filed_eng) components[i];

							if ("".equals(filed_eng.getText())) {
								JOptionPane.showMessageDialog(frame, "请先把当前内容填写完毕再增加查询字段信息！", "提示",
										JOptionPane.INFORMATION_MESSAGE);
								return false;
							}
						} else if ("Filed_cn".equals(components[i].getClass().getSimpleName())) {

							Filed_cn filed_cn = (Filed_cn) components[i];

							if ("".equals(filed_cn.getText())) {
								JOptionPane.showMessageDialog(frame, "请先把当前内容填写完毕再增加查询字段信息！", "提示",
										JOptionPane.INFORMATION_MESSAGE);
								return false;
							}
						} else if ("ServiceTypeComboBox".equals(components[i].getClass().getSimpleName())) {

							@SuppressWarnings("unchecked")
							ServiceTypeComboBox<String> serviceTypeComboBox = (ServiceTypeComboBox<String>) components[i];

							if ("布尔".equals(serviceTypeComboBox.getSelectedItem())
									|| "状态码".equals(serviceTypeComboBox.getSelectedItem())) {

								FiledComment filedComment = (FiledComment) components[i + 1];

								if ("".equals(filedComment.getText())) {

									JOptionPane.showMessageDialog(frame, "布尔或状态码的类型描述不能为空！！详细请见配置说明！", "提示",
											JOptionPane.INFORMATION_MESSAGE);
									return false;
								}

							}
						}

					}

				}

				return true;
			}

			private void addQueryMethod(JPanel querysPanel) {

				Set<String> tables = tableMap.keySet();
				String[] tableArr = tables.toArray(new String[] {});

				// 表名
				JComboBox<String> tableName = new JComboBox<>(tableArr);

				// 字段
				JTextField tableFiled = new Filed_eng();

				// 中文名称
				JTextField tableFiled_cn = new Filed_cn();

				// 字段类型
				JComboBox<String> filedType = new ServiceTypeComboBox<>();

				String[] typeList = new String[] { "字符串", "数字", "布尔", "状态码", "日期", "文字域" };

				filedType.setModel(new DefaultComboBoxModel<String>(typeList));

				// 类型描述
				JTextField filedComment = new FiledComment();

				// 是否可排序
				JComboBox<String> comboBox = new OrderComboBox<String>();
				comboBox.setModel(new DefaultComboBoxModel<String>(new String[] { "否", "是" }));

				querysPanel.add(tableName);
				querysPanel.add(tableFiled);
				querysPanel.add(tableFiled_cn);
				querysPanel.add(filedType);
				querysPanel.add(filedComment);
				querysPanel.add(comboBox);
			}

		});

		deleteQuery.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				//懒得抽方法了，冗余就冗余吧。。。
				
				// 获取容器个数
				int componentCount = querysPanel.getComponentCount();
				
				if (componentCount == 0) {
					return;
				}
				// 一行有6个
				// 如果是12，全部移除
				if (componentCount == 12) {
					querysPanel.removeAll();
					// 否则，移除最后6个
				} else {
					querysPanel.remove(componentCount - 1);
					querysPanel.remove(componentCount - 2);
					querysPanel.remove(componentCount - 3);
					querysPanel.remove(componentCount - 4);
					querysPanel.remove(componentCount - 5);
					querysPanel.remove(componentCount - 6);
				}

				frame.validate();
				frame.repaint();
			}
		});
		
		addCondition.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (!canAddCondition(conditionPanel)) {
					return;
				}

				if (conditionPanel.getComponentCount() == 0) {
					for (String conditionArr : conditionsArr) {
						JLabel label = new JLabel(conditionArr);
						conditionPanel.add(label);
					}
					addConditionMethod(conditionPanel);
					conditionPanel.getComponent(8).setVisible(false);
				} else {
					addConditionMethod(conditionPanel);
				}

				frame.validate();
				frame.repaint();
			}

			private boolean canAddCondition(JPanel conditionPanel) {

				if (conditionPanel.getComponentCount() >= 16) {

					Component[] components = conditionPanel.getComponents();

					for (int i = 8; i < components.length; i++) {

						if ("Filed_eng".equals(components[i].getClass().getSimpleName())) {

							Filed_eng filed_eng = (Filed_eng) components[i];

							if ("".equals(filed_eng.getText())) {
								JOptionPane.showMessageDialog(frame, "请先把当前内容填写完毕再增加条件字段信息！", "提示",
										JOptionPane.INFORMATION_MESSAGE);
								return false;
							}
						} else if ("Filed_cn".equals(components[i].getClass().getSimpleName())) {

							Filed_cn filed_cn = (Filed_cn) components[i];

							if ("".equals(filed_cn.getText())) {
								JOptionPane.showMessageDialog(frame, "请先把当前内容填写完毕再增加条件字段信息！", "提示",
										JOptionPane.INFORMATION_MESSAGE);
								return false;
							}
						} else if ("ServiceTypeComboBox".equals(components[i].getClass().getSimpleName())) {

							@SuppressWarnings("unchecked")
							ServiceTypeComboBox<String> serviceTypeComboBox = (ServiceTypeComboBox<String>) components[i];

							if ("布尔".equals(serviceTypeComboBox.getSelectedItem())
									|| "状态码".equals(serviceTypeComboBox.getSelectedItem())) {

								FiledComment filedComment = (FiledComment) components[i + 1];

								if ("".equals(filedComment.getText())) {

									JOptionPane.showMessageDialog(frame, "布尔或状态码的类型描述不能为空！！详细请见配置说明！", "提示",
											JOptionPane.INFORMATION_MESSAGE);
									return false;
								}

							}
						}

					}
				}

				return true;
			}

			private void addConditionMethod(JPanel conditionsPanel) {

				// 关联关系
				JComboBox<String> relationComBox = new RelationJcombox<>();

				relationComBox.setModel(new DefaultComboBoxModel<>(new String[] { "and", "or" }));

				// 表名
				Set<String> tables = tableMap.keySet();
				String[] tableArr = tables.toArray(new String[] {});
				JComboBox<String> tableName = new JComboBox<>(tableArr);

				// 字段
				JTextField tableFiled = new Filed_eng();

				// 中文名称
				JTextField tableFiled_cn = new Filed_cn();

				// 字段类型
				JComboBox<String> filedType = new ServiceTypeComboBox<>();

				String[] typeList = new String[] { "字符串", "数字", "布尔", "状态码", "日期", "文字域" };

				filedType.setModel(new DefaultComboBoxModel<String>(typeList));

				FiledComment filedComment = new FiledComment();

				// 比较关系
				JComboBox<String> compareRelation = new CompareComboBox<>();

				compareRelation.setModel(
						new DefaultComboBoxModel<>(new String[] { "=", "like", ">=", "<=", "!=", ">= && <=" }));

				// 固定值
				JTextField unchangeValue = new JTextField();

				conditionsPanel.add(relationComBox);
				conditionsPanel.add(tableName);
				conditionsPanel.add(tableFiled);
				conditionsPanel.add(tableFiled_cn);
				conditionsPanel.add(filedType);
				conditionsPanel.add(filedComment);
				conditionsPanel.add(compareRelation);
				conditionsPanel.add(unchangeValue);
			}
		});

		//删除条件字段
		deleteCondition.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				// 获取容器个数
				int componentCount = conditionPanel.getComponentCount();
				
				if (componentCount == 0) {
					return;
				}
				// 一行有8个
				// 如果是16，全部移除
				if (componentCount == 16) {
					conditionPanel.removeAll();
					// 否则，移除最后6个
				} else {
					conditionPanel.remove(componentCount - 1);
					conditionPanel.remove(componentCount - 2);
					conditionPanel.remove(componentCount - 3);
					conditionPanel.remove(componentCount - 4);
					conditionPanel.remove(componentCount - 5);
					conditionPanel.remove(componentCount - 6);
					conditionPanel.remove(componentCount - 7);
					conditionPanel.remove(componentCount - 8);
				}

				frame.validate();
				frame.repaint();
				
			}
		});
		
		restart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				tablesPanel.removeAll();
				querysPanel.removeAll();
				conditionPanel.removeAll();

				frame.validate();
				frame.repaint();

			}
		});

		confirm.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				String modelName = currentModelName.getText();

				String modelNameCn = currentModelName_cn.getText();

				// 当前模块方法名
				String methodName = methodNameField.getText();

				// 当前模块的方法中文名称
				String methodName_cn = methodName_cnField.getText();

				// 当前的实体名称
				String entityName = entityNameField.getText();

				// 当前实体的中文名称
				String entityName_cn = entityName_cnField.getText();

				if ("".equals(modelName) || "".equals(methodName) || "".equals(entityName)) {
					JOptionPane.showMessageDialog(frame, "模块名，方法名，实体名不可以为空！请填写！", "错误", JOptionPane.ERROR_MESSAGE);
					return;
				}

				// 为中文模块名赋值
				modelNameCn = "".equals(modelNameCn) ? modelName : modelNameCn;

				// 为中文方法名赋值
				methodName_cn = "".equals(methodName_cn) ? methodName : methodName_cn;

				// 为实体中文名称赋值
				entityName_cn = "".equals(entityName_cn) ? entityName : entityName_cn;

				// 检查关联表部分的填写情况
				if (!checkRelTable(tablesPanel, tableRelationModelLists, tableMap)) {
					return;
				}

				// 开始拼sql。。。
				String selectTableSql = "";

				String unsualFix = "";

				if ("mysql".equals(parameters.getDataBaseTypeVal())) {
					unsualFix = "`";
				} else if ("oracle".equals(parameters.getDataBaseTypeVal())) {
					unsualFix = "\"";
				}

				for (int i = 0; i < tableRelationModelLists.size(); i++) {

					TableRelationModel tableRelationModel = tableRelationModelLists.get(i);

					// 如果是第一个
					String anotherTableName = tableRelationModel.getAnotherTableName();

					String table2Text = tableRelationModel.getTable2_text();

					if (i == 0) {

						selectTableSql += " " + tableRelationModel.getRelation() + " " + unsualFix
								+ tableRelationModel.getTable1_text() + unsualFix + " " + anotherTableName + " ";
						continue;
					}

					selectTableSql += " " + tableRelationModel.getRelation() + " " + unsualFix
							+ tableRelationModel.getTable1_text() + unsualFix + " " + anotherTableName + " ON ";

					String table1Fileds = tableRelationModel.getTable1Fileds_text();
					String table2Fileds = tableRelationModel.getTable2Fileds_text();

					String[] table1FiledsArr = table1Fileds.split("&");
					String[] table2FiledsArr = table2Fileds.split("&");

					if (table1FiledsArr.length != table2FiledsArr.length) {
						JOptionPane.showMessageDialog(frame, "关联表配置中第  " + (i + 1) + " 行表1和表2的关联字段的个数不相等，请检查后重新配置！",
								"错误", JOptionPane.ERROR_MESSAGE);
						return;
					}

					for (int j = 0; j < table1FiledsArr.length; j++) {

						if (j == table1FiledsArr.length - 1) {
							selectTableSql += anotherTableName + "." + table1FiledsArr[j] + " = "
									+ tableMap.get(table2Text) + "." + table2FiledsArr[j];
							break;
						}

						selectTableSql += anotherTableName + "." + table1FiledsArr[j] + " = " + tableMap.get(table2Text)
								+ "." + table2FiledsArr[j] + " and ";

					}

				}

				// System.out.println(selectTableSql);

				// 查询部分
				Component[] queryComponents = querysPanel.getComponents();
				List<TableFiledModel> tableFiledModels = new ArrayList<>();
				// 查询panel的信息
				for (int i = 6; i < queryComponents.length; i++) {

					if ("JComboBox".equals(queryComponents[i].getClass().getSimpleName())) {

						@SuppressWarnings("unchecked")
						JComboBox<String> comboBox = (JComboBox<String>) queryComponents[i];

						TableFiledModel filedModel = new TableFiledModel();

						filedModel.setComboBox(comboBox);

						filedModel.setTableName((String) comboBox.getSelectedItem());

						filedModel.setAnotherTableName(tableMap.get(comboBox.getSelectedItem()));

						tableFiledModels.add(filedModel);
					} else if ("Filed_eng".equals(queryComponents[i].getClass().getSimpleName())) {
						Filed_eng filed_eng = (Filed_eng) queryComponents[i];

						String engFiled = filed_eng.getText();

						if ("".equals(engFiled)) {
							JOptionPane.showMessageDialog(frame, "添加查询字段中的字段列不能为空！请填写！！", "错误",
									JOptionPane.ERROR_MESSAGE);
							return;
						}

						TableFiledModel tableFiledModel = tableFiledModels.get(tableFiledModels.size() - 1);

						tableFiledModel.setFiled_eng(filed_eng);
						tableFiledModel.setFiledText_eng(engFiled);
						tableFiledModel.setAnotherFiledName(tableFiledModel.getAnotherTableName() + "_" + engFiled);
					} else if ("Filed_cn".equals(queryComponents[i].getClass().getSimpleName())) {

						Filed_cn filed_cn = (Filed_cn) queryComponents[i];

						String cnFiled = filed_cn.getText();

						if ("".equals(cnFiled)) {
							JOptionPane.showMessageDialog(frame, "添加查询字段中的中文名称列不能为空！请填写！！", "错误",
									JOptionPane.ERROR_MESSAGE);
							return;
						}

						tableFiledModels.get(tableFiledModels.size() - 1).setFiled_cn(filed_cn);
						tableFiledModels.get(tableFiledModels.size() - 1).setFiledText_cn(cnFiled);
					} else if ("ServiceTypeComboBox".equals(queryComponents[i].getClass().getSimpleName())) {

						@SuppressWarnings("unchecked")
						ServiceTypeComboBox<String> serviceTypeComboBox = (ServiceTypeComboBox<String>) queryComponents[i];

						String filedType = (String) serviceTypeComboBox.getSelectedItem();

						tableFiledModels.get(tableFiledModels.size() - 1).setServiceTypeComboBox(serviceTypeComboBox);
						tableFiledModels.get(tableFiledModels.size() - 1).setFiledType(filedType);

					} else if ("FiledComment".equals(queryComponents[i].getClass().getSimpleName())) {

						FiledComment filedComment = (FiledComment) queryComponents[i];

						String comment = filedComment.getText();

						String[] serviceArr = comment.split("#");

						TableFiledModel tableFiledModel = tableFiledModels.get(tableFiledModels.size() - 1);

						tableFiledModel.setTextFiled(filedComment);
						tableFiledModel.setFiledCommentStr(comment);

						String filedType = tableFiledModel.getFiledType();

						Map<String, Object> map = new HashMap<>();
						if ("布尔".equals(filedType)) {
							try {
								map.put("是", serviceArr[0]);
								map.put("否", serviceArr[1]);
								tableFiledModel.setFiledComment(map);
							} catch (Exception e1) {
								JOptionPane.showMessageDialog(frame, "布尔值的类型描述填写有误，请检查重写！", "错误",
										JOptionPane.ERROR_MESSAGE);
								return;
							}
						}

						if ("状态码".equals(filedType)) {
							try {
								for (String serviece : serviceArr) {
									String[] arr = serviece.split("&");
									map.put(arr[0], arr[1]);
								}
								tableFiledModel.setFiledComment(map);
							} catch (Exception e1) {
								JOptionPane.showMessageDialog(frame, "状态码的类型描述填写有误，请检查重写！", "错误",
										JOptionPane.ERROR_MESSAGE);
								return;
							}
						}
					} else if ("OrderComboBox".equals(queryComponents[i].getClass().getSimpleName())) {

						@SuppressWarnings("unchecked")
						OrderComboBox<String> orderComboBox = (OrderComboBox<String>) queryComponents[i];

						String canSort = (String) orderComboBox.getSelectedItem();

						tableFiledModels.get(tableFiledModels.size() - 1).setOrderComboBox(orderComboBox);

						tableFiledModels.get(tableFiledModels.size() - 1).setCanSort(canSort);

					}

				}

				if (tableFiledModels.size() == 0) {
					JOptionPane.showMessageDialog(frame, "请配置查询字段信息！", "错误", JOptionPane.ERROR_MESSAGE);
					return;
				}

				// 条件部分
				Component[] conditionComponents = conditionPanel.getComponents();

				List<TableConditionModel> tableConditionModels = new ArrayList<>();
				// 条件panel的信息
				for (int i = 6; i < conditionComponents.length; i++) {

					if ("RelationJcombox".equals(conditionComponents[i].getClass().getSimpleName())) {

						@SuppressWarnings("unchecked")
						RelationJcombox<String> comboBox = (RelationJcombox<String>) conditionComponents[i];

						TableConditionModel conditionModel = new TableConditionModel();

						conditionModel.setRelationJcombox(comboBox);
						conditionModel.setRelation((String) comboBox.getSelectedItem());

						tableConditionModels.add(conditionModel);

					} else if ("JComboBox".equals(conditionComponents[i].getClass().getSimpleName())) {

						@SuppressWarnings("unchecked")
						JComboBox<String> comboBox = (JComboBox<String>) conditionComponents[i];

						tableConditionModels.get(tableConditionModels.size() - 1).setComboBox(comboBox);
						// 设置表名
						tableConditionModels.get(tableConditionModels.size() - 1)
								.setTableName((String) comboBox.getSelectedItem());

						// 设置表别名
						tableConditionModels.get(tableConditionModels.size() - 1)
								.setAnotherTableName(tableMap.get((String) comboBox.getSelectedItem()));
					} else if ("Filed_eng".equals(conditionComponents[i].getClass().getSimpleName())) {
						Filed_eng filed_eng = (Filed_eng) conditionComponents[i];

						String engFiled = filed_eng.getText();

						if ("".equals(engFiled)) {
							JOptionPane.showMessageDialog(frame, "添加条件字段中的字段列不能为空！请填写！！", "错误",
									JOptionPane.ERROR_MESSAGE);
							return;
						}

						tableConditionModels.get(tableConditionModels.size() - 1).setFiled_eng_textFiled(filed_eng);
						tableConditionModels.get(tableConditionModels.size() - 1).setFiled_eng(engFiled);
					} else if ("Filed_cn".equals(conditionComponents[i].getClass().getSimpleName())) {

						Filed_cn filed_cn = (Filed_cn) conditionComponents[i];

						String cnFiled = filed_cn.getText();

						if ("".equals(cnFiled)) {
							JOptionPane.showMessageDialog(frame, "添加条件字段中的中文名称列不能为空！请填写！！", "错误",
									JOptionPane.ERROR_MESSAGE);
							return;
						}

						tableConditionModels.get(tableConditionModels.size() - 1).setFiled_cn_textFiled(filed_cn);
						tableConditionModels.get(tableConditionModels.size() - 1).setFiled_cn(cnFiled);
					} else if ("ServiceTypeComboBox".equals(conditionComponents[i].getClass().getSimpleName())) {

						@SuppressWarnings("unchecked")
						ServiceTypeComboBox<String> serviceTypeComboBox = (ServiceTypeComboBox<String>) conditionComponents[i];

						String filedType = (String) serviceTypeComboBox.getSelectedItem();

						tableConditionModels.get(tableConditionModels.size() - 1)
								.setServiceTypeComboBox(serviceTypeComboBox);
						tableConditionModels.get(tableConditionModels.size() - 1).setServiceType(filedType);

					} else if ("FiledComment".equals(conditionComponents[i].getClass().getSimpleName())) {

						FiledComment filedComment = (FiledComment) conditionComponents[i];

						String comment = filedComment.getText();

						String[] serviceArr = comment.split("#");

						TableConditionModel tableConditionModel = tableConditionModels
								.get(tableConditionModels.size() - 1);

						tableConditionModel.setFiledComment(filedComment);

						String filedType = tableConditionModel.getServiceType();

						Map<String, Object> map = new HashMap<>();
						if ("布尔".equals(filedType)) {
							try {
								map.put("是", serviceArr[0]);
								map.put("否", serviceArr[1]);
								tableConditionModel.setServiceText(map);
							} catch (Exception e1) {
								JOptionPane.showMessageDialog(frame, "布尔值的类型描述填写有误，请检查重写！", "错误",
										JOptionPane.ERROR_MESSAGE);
								return;
							}
						}

						if ("状态码".equals(filedType)) {
							try {
								for (String serviece : serviceArr) {
									String[] arr = serviece.split("&");
									map.put(arr[0], arr[1]);
								}
								tableConditionModel.setServiceText(map);
							} catch (Exception e1) {
								JOptionPane.showMessageDialog(frame, "状态码的类型描述填写有误，请检查重写！", "错误",
										JOptionPane.ERROR_MESSAGE);
								return;
							}
						}
					} else if ("CompareComboBox".equals(conditionComponents[i].getClass().getSimpleName())) {

						@SuppressWarnings("unchecked")
						CompareComboBox<String> compareComboBox = (CompareComboBox<String>) conditionComponents[i];

						String compareText = (String) compareComboBox.getSelectedItem();

						tableConditionModels.get(tableConditionModels.size() - 1).setCompareComboBox(compareComboBox);
						tableConditionModels.get(tableConditionModels.size() - 1).setCompareText(compareText);

					} else if ("JTextField".equals(conditionComponents[i].getClass().getSimpleName())) {

						JTextField textField = (JTextField) conditionComponents[i];

						String unchangeValue = textField.getText();

						tableConditionModels.get(tableConditionModels.size() - 1).setTextFiled(textField);
						tableConditionModels.get(tableConditionModels.size() - 1).setUnchangeValue(unchangeValue);

					}

				}

				if (tableConditionModels.size() == 0) {
					JOptionPane.showMessageDialog(frame, "请配置条件字段信息！", "错误", JOptionPane.ERROR_MESSAGE);
					return;
				}

				// 多表配置对象
				TablesQueryModel tablesQueryModel = new TablesQueryModel();

				// 储存多表查询的信息配置
				tablesQueryModel.setFormSql(selectTableSql);
				List<TableRelationModel> realTableRelationModelLists = new ArrayList<>();
				realTableRelationModelLists.addAll(tableRelationModelLists);
				tablesQueryModel.setTableRelationModelLists(realTableRelationModelLists);
				tablesQueryModel.setTableFiledModels(tableFiledModels);
				tablesQueryModel.setTableConditionModels(tableConditionModels);

				// 如果当前模块存在当前配置的方法名称

				Map<String, TablesQueryModel> methodMap = ChildWindowConstant.tablesQueryMap.get(modelName);

				if (ChildWindowConstant.tablesQueryMap.containsKey(modelName)) {

					// 如果已经配置了同名的方法，询问时候覆盖
					if (methodMap.containsKey(methodName)) {
						int getCodeConfirmDialog = JOptionPane.showConfirmDialog(frame,
								"当前模块‘" + modelName + "’下的方法‘" + methodName + "’已存在，确认覆盖？", "提示",
								JOptionPane.YES_NO_OPTION);

						if (getCodeConfirmDialog != 0) {
							return;
						}
					}
				}

				// 如果当前模块下没有配置任何method

				if (methodMap == null) {

					methodMap = new HashMap<>();
				}

				tablesQueryModel.setMethodName_cn(methodName_cn);
				tablesQueryModel.setEntityName(entityName);
				tablesQueryModel.setEntityName_cn(entityName_cn);

				// 存储方法对应的sql
				methodMap.put(methodName, tablesQueryModel);

				// 添加到当前模块中
				ChildWindowConstant.tablesQueryMap.put(modelName, methodMap);
				// 存储当前模块的英文名和中文名
				ChildWindowConstant.tablesQueryEndAndCnMap.put(modelName, modelNameCn);

				JOptionPane.showMessageDialog(frame, "当前模块配置成功！", "提示", JOptionPane.INFORMATION_MESSAGE);

				String[] modelArr = ChildWindowConstant.tablesQueryMap.keySet().toArray(new String[] {});

				tableConfigExit.setModel(new DefaultComboBoxModel<>(new String[] { "--请选择--" }));

				methodListCombox.setModel(new DefaultComboBoxModel<>(new String[] { "--请选择--" }));

				for (String model : modelArr) {
					tableConfigExit.addItem(model);
				}

				currentModelName.setText("");
				currentModelName_cn.setText("");
				methodNameField.setText("");
				methodName_cnField.setText("");
				entityNameField.setText("");
				entityName_cnField.setText("");

				tablesPanel.removeAll();
				querysPanel.removeAll();
				conditionPanel.removeAll();

				addQuery.setEnabled(false);
				addCondition.setEnabled(false);

				frame.validate();
				frame.repaint();

			}
		});

		confirmRelTable.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (!checkRelTable(tablesPanel, tableRelationModelLists, tableMap)) {
					return;
				}

				addQuery.setEnabled(true);
				addCondition.setEnabled(true);

				Component[] queryComponents = querysPanel.getComponents();

				for (int i = 6; i < queryComponents.length; i++) {

					if ("JComboBox".equals(queryComponents[i].getClass().getSimpleName())) {

						@SuppressWarnings("unchecked")
						JComboBox<String> comboBox = (JComboBox<String>) queryComponents[i];

						Set<String> tables = tableMap.keySet();

						comboBox.removeAllItems();

						for (String table : tables) {
							comboBox.addItem(table);
						}

					}
				}

				Component[] conditionComponents = conditionPanel.getComponents();

				for (int i = 8; i < conditionComponents.length; i++) {

					if ("JComboBox".equals(conditionComponents[i].getClass().getSimpleName())) {

						@SuppressWarnings("unchecked")
						JComboBox<String> comboBox = (JComboBox<String>) conditionComponents[i];

						Set<String> tables = tableMap.keySet();

						comboBox.removeAllItems();

						for (String table : tables) {
							comboBox.addItem(table);
						}

					}
				}

				frame.validate();
				frame.repaint();

			}
		});

		// 配置模块下拉框监听
		tableConfigExit.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {

				if (e.getStateChange() != 1) {
					return;
				}

				// 选取的当前模块
				String currentModel = tableConfigExit.getSelectedItem().toString();

				if ("--请选择--".equals(currentModel)) {

					tablesPanel.removeAll();
					querysPanel.removeAll();
					conditionPanel.removeAll();

					currentModelName.setText("");
					currentModelName_cn.setText("");
					methodNameField.setText("");
					methodName_cnField.setText("");
					entityNameField.setText("");
					entityName_cnField.setText("");
					methodListCombox.setModel(new DefaultComboBoxModel<>(new String[] { "--请选择--" }));

					frame.validate();
					frame.repaint();

					return;

				}

				currentModelName.setText(currentModel);

				String currentCnModelName = ChildWindowConstant.tablesQueryEndAndCnMap.get(currentModel);

				currentModelName_cn.setText(currentCnModelName);

				// 获取对应的方法列表
				String[] methods = ChildWindowConstant.tablesQueryMap.get(currentModel).keySet()
						.toArray(new String[] {});

				// 根据当前模块设置方法
				methodListCombox.setModel(new DefaultComboBoxModel<>(new String[] { "--请选择--" }));

				for (String methodName : methods) {
					methodListCombox.addItem(methodName);
				}

				frame.validate();
				frame.repaint();

			}

		});

		// 方法下拉框监听
		methodListCombox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {

				if (e.getStateChange() != 1) {
					return;
				}

				// 选取的当前方法名
				String currentMethod = methodListCombox.getSelectedItem().toString();

				if ("--请选择--".equals(currentMethod)) {

					tablesPanel.removeAll();
					querysPanel.removeAll();
					conditionPanel.removeAll();

					methodNameField.setText("");
					methodName_cnField.setText("");

					frame.validate();
					frame.repaint();

					return;

				}

				// 选取的模块名
				String currentModel = tableConfigExit.getSelectedItem().toString();

				// 选择的当前方法名
				TablesQueryModel tablesQueryModel = ChildWindowConstant.tablesQueryMap.get(currentModel)
						.get(currentMethod);
				// 动态设置方法名称和中文名称
				methodNameField.setText(currentMethod);
				methodName_cnField.setText(tablesQueryModel.getMethodName_cn());
				entityNameField.setText(tablesQueryModel.getEntityName());
				entityName_cnField.setText(tablesQueryModel.getEntityName_cn());

				List<TableRelationModel> tableRelations = tablesQueryModel.getTableRelationModelLists();

				List<TableFiledModel> tableFileds = tablesQueryModel.getTableFiledModels();

				List<TableConditionModel> tableConditions = tablesQueryModel.getTableConditionModels();

				// 移除三个panel所有的组件
				tablesPanel.removeAll();
				querysPanel.removeAll();
				conditionPanel.removeAll();

				// 加入表头和内容
				for (String tableStr : tablesArr) {
					JLabel label = new JLabel(tableStr);
					tablesPanel.add(label);
				}
				for (TableRelationModel tableRelation : tableRelations) {

					tablesPanel.add(tableRelation.getComboBox());
					tablesPanel.add(tableRelation.getTable1());
					tablesPanel.add(tableRelation.getTable2());
					tablesPanel.add(tableRelation.getTable1Fileds());
					tablesPanel.add(tableRelation.getTable2Fileds());

				}

				for (String queryStr : querysArr) {
					JLabel label = new JLabel(queryStr);
					querysPanel.add(label);
				}
				for (TableFiledModel tableFiled : tableFileds) {

					querysPanel.add(tableFiled.getComboBox());
					querysPanel.add(tableFiled.getFiled_eng());
					querysPanel.add(tableFiled.getFiled_cn());
					querysPanel.add(tableFiled.getServiceTypeComboBox());
					querysPanel.add(tableFiled.getTextFiled());
					querysPanel.add(tableFiled.getOrderComboBox());

				}

				for (String conditionStr : conditionsArr) {
					JLabel label = new JLabel(conditionStr);
					conditionPanel.add(label);
				}
				for (TableConditionModel tableCondition : tableConditions) {

					conditionPanel.add(tableCondition.getRelationJcombox());
					conditionPanel.add(tableCondition.getComboBox());
					conditionPanel.add(tableCondition.getFiled_eng_textFiled());
					conditionPanel.add(tableCondition.getFiled_cn_textFiled());
					conditionPanel.add(tableCondition.getServiceTypeComboBox());
					conditionPanel.add(tableCondition.getFiledComment());
					conditionPanel.add(tableCondition.getCompareComboBox());
					conditionPanel.add(tableCondition.getTextFiled());

				}

				frame.validate();
				frame.repaint();

			}
		});

		frame.getContentPane().setLayout(groupLayout);
	}

	private boolean checkRelTable(JPanel tablesPanel, List<TableRelationModel> tableRelationModelLists,
			Map<String, String> tableMap) {

		// 清空集合
		tableRelationModelLists.clear();
		tableMap.clear();

		Component[] tableComponents = tablesPanel.getComponents();

		// 读取关联表的配置信息
		for (int i = 4; i < tableComponents.length; i++) {

			if ("JComboBox".equals(tableComponents[i].getClass().getSimpleName())) {

				@SuppressWarnings("unchecked")
				JComboBox<String> comboBox = (JComboBox<String>) tableComponents[i];

				TableRelationModel tableRelationModel = new TableRelationModel();

				tableRelationModel.setComboBox(comboBox);

				tableRelationModel.setRelation((String) comboBox.getSelectedItem());

				tableRelationModelLists.add(tableRelationModel);

			} else if ("Table1".equals(tableComponents[i].getClass().getSimpleName())) {
				Table1 table1 = (Table1) tableComponents[i];

				tableRelationModelLists.get(tableRelationModelLists.size() - 1).setTable1(table1);

				tableRelationModelLists.get(tableRelationModelLists.size() - 1).setTable1_text(table1.getText());

				// 表别名
				tableRelationModelLists.get(tableRelationModelLists.size() - 1)
						.setAnotherTableName("t" + (tableRelationModelLists.size() - 1));

			} else if ("Table2".equals(tableComponents[i].getClass().getSimpleName())) {

				Table2 table2 = (Table2) tableComponents[i];

				tableRelationModelLists.get(tableRelationModelLists.size() - 1).setTable2(table2);

				tableRelationModelLists.get(tableRelationModelLists.size() - 1)
						.setTable2_text((String) table2.getSelectedItem());

			} else if ("Table1Fileds".equals(tableComponents[i].getClass().getSimpleName())) {

				Table1Fileds table1Fileds = (Table1Fileds) tableComponents[i];

				tableRelationModelLists.get(tableRelationModelLists.size() - 1).setTable1Fileds(table1Fileds);

				tableRelationModelLists.get(tableRelationModelLists.size() - 1)
						.setTable1Fileds_text(table1Fileds.getText());

			} else if ("Table2Fileds".equals(tableComponents[i].getClass().getSimpleName())) {

				Table2Fileds table2Fileds = (Table2Fileds) tableComponents[i];

				tableRelationModelLists.get(tableRelationModelLists.size() - 1).setTable2Fileds(table2Fileds);

				tableRelationModelLists.get(tableRelationModelLists.size() - 1)
						.setTable2Fileds_text(table2Fileds.getText());

			}
		}

		// 如果集合大小为0，说明没配
		if (tableRelationModelLists.size() == 0) {

			JOptionPane.showMessageDialog(frame, "请先配置关联表信息！！！", "提示", JOptionPane.INFORMATION_MESSAGE);
			return false;

		}

		// 检查关联表配置有没有字段为空
		for (int i = 0; i < tableRelationModelLists.size(); i++) {

			TableRelationModel tableRelationModel = tableRelationModelLists.get(i);

			if ("".equals(tableRelationModel.getTable1_text()) || "".equals(tableRelationModel.getTable1Fileds_text())
					|| "".equals(tableRelationModel.getTable2Fileds_text())) {
				JOptionPane.showMessageDialog(frame, "表1，表1字段，表2字段不能为空！！请填写！！", "错误", JOptionPane.ERROR_MESSAGE);
				return false;

			}

			// 动态表名称使用
			tableMap.put(tableRelationModel.getTable1_text(), tableRelationModel.getAnotherTableName());

		}
		return true;

	}
}
