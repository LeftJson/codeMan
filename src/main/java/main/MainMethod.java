package main;

import constant.ChildWindowConstant;
import constant.CodeConstant;
import constant.Constant;
import entity.Parameters;
import freemarker.template.TemplateException;
import util.CodeWriterUtil;
import util.FreeOutUtil;
import util.ZipUtils;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class MainMethod {
	public static boolean progress(Parameters parameters) {
		String projectNameVal = parameters.getProjectNameVal();
		// projectNameVal = projectNameVal.toLowerCase();
		// 首字母大写的projectNameVal
		String captureProjectNameVal = CodeWriterUtil.captureName(projectNameVal);
		// 所选框架
		String frameWorkVal = parameters.getFrameWorkVal();
		String jsFrameWork = parameters.getJsFrameWork();
		String clientStyle = parameters.getClientFrameWorkVal();
		String clientStyleVal = clientStyle.split("-")[0];
		String dataBasePwdVal = parameters.getDataBasePwdVal();
		String dataBaseUserNameVal = parameters.getDataBaseUserNameVal();
		String dataBaseTypeVal = parameters.getDataBaseTypeVal();
		// 表名
		String tableNameVal = parameters.getTableName();
		String outPathVal = parameters.getOutPathVal();
		// 数据库url
		String dataBaseUrlVal = parameters.getDataBaseUrl();
		// 数据库driverClass
		String dataBaseDriverClassVal = parameters.getDataBaseDriverClass();
		// 登录选择
		String loginModel = parameters.getMakeModelVal();
		// 主题
		String themeVal = parameters.getThemeVal();
		try {
			// 解压
			ZipUtils.unZip(new File(Constant.modelFilesZip), Constant.modelFiles, Constant.zipPwd);
			// =============================================JavaBean==========================================
			String ifJavaBean = ChildWindowConstant.tableParamConfig.get(CodeConstant.paramConfigKey);
			// dao,controller,service and 公共
			Map<String, Object> root = new HashMap<>();
			// 项目名称
			root.put(CodeConstant.projectName, projectNameVal);
			// 首字母大写的项目名
			root.put(CodeConstant.captureProjectName, captureProjectNameVal);
			root.put(CodeConstant.packageName, projectNameVal);
			// 框架
			root.put(CodeConstant.frameWorkVal, frameWorkVal);
			root.put(CodeConstant.jsFrameWork, jsFrameWork);
			root.put(CodeConstant.theme, themeVal);
			root.put(CodeConstant.clientStyleVal, clientStyleVal);
			// yml
			root.put(CodeConstant.dataBaseUserName, dataBaseUserNameVal);
			root.put(CodeConstant.dataBasePwd, dataBasePwdVal);
			root.put(CodeConstant.dataBaseUrl, dataBaseUrlVal);
			root.put(CodeConstant.dataBaseDriverClass, dataBaseDriverClassVal);
			// sqlMapper
			root.put(CodeConstant.dataBaseType, dataBaseTypeVal);
			root.put(CodeConstant.loginModel, loginModel);
			// 连接池
			root.put(CodeConstant.databasePool, ChildWindowConstant.commonParametersModel.getDatabasePool());
			// swagger2
			root.put(CodeConstant.ifUseSwagger, ChildWindowConstant.commonParametersModel.getIfUseSwagger());
			// --------------------
			// 设置登录用户
			CodeWriterUtil.setLonginUser(loginModel, root);
			// --------------------
			// 项目路径配置
			String constantDir = FreeOutUtil.setProjectName(Constant.freeOutPath_constant, projectNameVal);
			String entityDir = FreeOutUtil.setProjectName(Constant.freeOutPath_entity, projectNameVal);
			String daoDir = FreeOutUtil.setProjectName(Constant.freeOutPath_dao, projectNameVal);
			String IServiceDir = FreeOutUtil.setProjectName(Constant.freeOutPath_service, projectNameVal);
			String serviceDir = FreeOutUtil.setProjectName(Constant.freeOutPath_serviceImpl, projectNameVal);
			String controllerDir = FreeOutUtil.setProjectName(Constant.freeOutPath_controller, projectNameVal);
			// 静态资源文件，包括yml等
			String resourcesDir = FreeOutUtil.setProjectName(Constant.freeOutPath_resources, projectNameVal);
			String sqlMapperDir = null;
			String htmlDir = null;
			// 入口文件的路径
			String mainApplicationDir = null;
			// sqlMapper和html的路径
			if ("ssm".equals(frameWorkVal)) {
				sqlMapperDir = FreeOutUtil.setProjectName(Constant.freeOutPath_sqlMapper_MVC, projectNameVal);
				htmlDir = FreeOutUtil.setProjectName(Constant.freeOutPath_WEBINF_MVC, projectNameVal);
			} else if ("springBoot".equals(frameWorkVal)) {
				sqlMapperDir = FreeOutUtil.setProjectName(Constant.freeOutPath_sqlMapper, projectNameVal);
				htmlDir = FreeOutUtil.setProjectName(Constant.freeOutPath_templates, projectNameVal);
				// 入口文件的路径
				mainApplicationDir = FreeOutUtil.setProjectName(Constant.mainApplicationPath, projectNameVal);
			}
			// 测试入口文件的路径
			String mainApplicationTestDir = FreeOutUtil.setProjectName(Constant.testPath, projectNameVal);
			// pom.xml的路径 .project .classpath
			String projectDir = FreeOutUtil.setProjectName(Constant.freeOutPath, projectNameVal);
			// 工具类文件的路径
			String utilDir = FreeOutUtil.setProjectName(Constant.utilPath, projectNameVal);
			// Config的路径
			String mvcConfigDir = FreeOutUtil.setProjectName(Constant.mvcConfig, projectNameVal);
			String[] tableNameArr = new String[]{};
			// 开始数据库处理===============
			if (!"".equals(tableNameVal)) {
				tableNameArr = tableNameVal.split("#");
			}
			// 生成home页面和mvc配置文件使用
			root.put(CodeConstant.tableNameList, ChildWindowConstant.currentTableCnNameMap);
			// 多表配置模块
			if (ChildWindowConstant.tablesQueryMap.size() != 0) {
				root.put(CodeConstant.tablesQueryMap, ChildWindowConstant.tablesQueryMap);
				root.put(CodeConstant.tablesQueryEndAndCnMap, ChildWindowConstant.tablesQueryEndAndCnMap);
			}
			// 创建几个核心包
			FreeOutUtil.checkAndMakeDir(outPathVal + constantDir);
			FreeOutUtil.checkAndMakeDir(outPathVal + entityDir);
			FreeOutUtil.checkAndMakeDir(outPathVal + daoDir);
			FreeOutUtil.checkAndMakeDir(outPathVal + IServiceDir);
			FreeOutUtil.checkAndMakeDir(outPathVal + serviceDir);
			FreeOutUtil.checkAndMakeDir(outPathVal + controllerDir);
			FreeOutUtil.checkAndMakeDir(outPathVal + sqlMapperDir);
			FreeOutUtil.checkAndMakeDir(outPathVal + htmlDir);
			// 如果是前后端分离，还需要额外创建${projectName}-webClient包，即前台页面包
			String webClientPath = "";
			String webClientHtmlPath = "";
			String webClientJsConfigPath = "";
			if (CodeConstant.hAdminTheme.equals(themeVal)) {
				webClientPath = FreeOutUtil.setWebClientName(Constant.webClientPath, projectNameVal);
				webClientPath = FreeOutUtil.checkAndMakeDir(outPathVal + webClientPath);
				webClientHtmlPath = FreeOutUtil.setWebClientName(Constant.webClientHtmlPath, projectNameVal);
				webClientHtmlPath = FreeOutUtil.checkAndMakeDir(outPathVal + webClientHtmlPath);
				webClientJsConfigPath = FreeOutUtil.setWebClientName(Constant.webClientJsConfigPath, projectNameVal);
				webClientJsConfigPath = FreeOutUtil.checkAndMakeDir(outPathVal + webClientJsConfigPath);
			}
			// 单表代码生成
			if (!CodeWriterUtil.getSingleTableCode(outPathVal, ifJavaBean, entityDir, daoDir, IServiceDir, serviceDir,
					controllerDir, sqlMapperDir, themeVal, htmlDir, webClientHtmlPath, tableNameArr, dataBaseTypeVal, root)) {
				return false;
			}
			//多表代码生成
			if (!CodeWriterUtil.getMutiTableCode(outPathVal, sqlMapperDir, IServiceDir, serviceDir, daoDir,
					controllerDir, themeVal, entityDir, htmlDir, webClientHtmlPath, root)) {
				return false;
			}
			// 生成自定义实体
			CodeWriterUtil.getMakeEntity(outPathVal, entityDir, root);
			// 下载setting.zip文件到项目路径下
			String settingsDir = FreeOutUtil.setProjectName(Constant.freeOutPath_settings, projectNameVal);
			if (!CodeWriterUtil.getSettingCode(outPathVal, frameWorkVal, settingsDir)) {
				return false;
			}
			String staticDir = null;
			// 如果是old 下载static.zip文件到项目路径下
			if (CodeConstant.oldTheme.equals(themeVal)) {
				if ("ssm".equals(frameWorkVal)) {
					staticDir = FreeOutUtil.setProjectName(Constant.freeOutPath_static_MVC, projectNameVal);
				}
				if ("springBoot".equals(frameWorkVal)) {
					staticDir = FreeOutUtil.setProjectName(Constant.freeOutPath_static, projectNameVal);
				}
			}
			if (!CodeWriterUtil.getStaticCode(outPathVal, themeVal, staticDir, webClientPath)) {
				return false;
			}
			if (!CodeWriterUtil.getOjdbcCode(outPathVal, frameWorkVal, dataBaseTypeVal, projectNameVal)) {
				return false;
			}
			//生成公共类
			CodeWriterUtil.getCommonCode(outPathVal, projectDir, utilDir, themeVal, htmlDir,
					webClientHtmlPath, daoDir, resourcesDir, sqlMapperDir, controllerDir, mvcConfigDir, entityDir,
					constantDir, staticDir, webClientJsConfigPath, root);
			//生成动态用户的相关代码
			CodeWriterUtil.getDynamicUser(outPathVal, sqlMapperDir, IServiceDir, serviceDir, daoDir, loginModel, root);
			//生成springboot的公共代码
			CodeWriterUtil.getSpringbootCommon(outPathVal, resourcesDir, mainApplicationDir, captureProjectNameVal,
					mainApplicationTestDir, mvcConfigDir, root, frameWorkVal);
			//生成springmvc的公共代码
			CodeWriterUtil.getSpringMvcCommon(outPathVal, resourcesDir, projectNameVal, htmlDir, settingsDir, mvcConfigDir, root, frameWorkVal);
		} catch (IOException e) {
			JOptionPane.showMessageDialog(Constant.frmv, "生成代码时出错！", "错误", JOptionPane.ERROR_MESSAGE);
			return false;
		} catch (TemplateException e) {
			JOptionPane.showMessageDialog(Constant.frmv, "解析模板文件时出错！", "错误", JOptionPane.ERROR_MESSAGE);
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(Constant.frmv, "生成代码时出错！", "错误", JOptionPane.ERROR_MESSAGE);
			return false;
		} finally {
			// 删除model文件
			ZipUtils.deleteFiles(Constant.modelFiles);
		}
		return true;
	}
}
