<#if theme == "经典后台Thymleaf版">
<#if jsFrameWork == "jquery">
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">

<head>
  <meta charset="UTF-8">
  <title>登录</title>
  
  <link rel="stylesheet" th:href="@{/mystatic/bootstrap/css/bootstrap.min-${clientStyleVal}.css}">
  <link rel="stylesheet" th:href="@{/mystatic/css/style.css}">
  <script>if(window.top !== window.self){ window.top.location = window.location;}</script>
  
</head>

<body>

    <div class="wrapper" id="loginModel">
		<form class="form-signin">
			<h2 class="form-signin-heading">请登录</h2>
			<input type="text" class="form-control" 
				placeholder="用户名" required="" autofocus="" id='userName'/> <input
				type="password" class="form-control" 
				placeholder="密码" required="" id='password'/> <input type="checkbox"
				value="remember-me" id="rememberMe">
			记住我<br /> <br />
			<button class="btn btn-lg btn-primary btn-block" type="button" id="loginBT">登录</button>
		</form>
	</div>
  
<script th:src="@{/mystatic/jquery/jquery.min.js}"></script>
<script th:src="@{/mystatic/js/jqAlert.js}"></script>
<script th:src="@{/mystatic/js/ajaxFactory.js}"></script>
<script th:src="@{/mystatic/js/config.js}"></script>

<script>

	var user = localStorage.getItem("user");

	if (user != null && user != undefined) {
        try {
            user = JSON.parse(user);
            $("#userName").val(user.userName);
            $("#password").val(user.password);
        } catch(err){}
	} 

	$("#loginBT").on("click",function() {
		
		$z.ajaxStrAndJson({
			url : basePath + "/login/doLogin",
			data : {
				${userNameFiled} : $("#userName").val(),
				${userPwdFiled} : $("#password").val()
			},
			success : function(data) {
				$z.dealCommonResult(data, function() {
					var rememberMe = $("#rememberMe").prop("checked");
					//如果点了记住我，则存储到本地
					if (rememberMe) {
	                    localStorage.setItem("user", JSON.stringify({
	                        userName: $("#userName").val(),
	                        password: $("#password").val()
	                    }));
					}
					window.location.href = basePath + '/home';
				});
	
			}
		});
		
	
	} );
	
</script>

</body>

</html>
</#if>

<#if jsFrameWork == "vue">
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">

<head>
  <meta charset="UTF-8">
  <title>登录</title>
  
  <link rel="stylesheet" th:href="@{/mystatic/bootstrap/css/bootstrap.min-${clientStyleVal}.css}">
  <link rel="stylesheet" th:href="@{/mystatic/css/style.css}">
  <script>if(window.top !== window.self){ window.top.location = window.location;}</script>
  
</head>

<body>

    <div class="wrapper" id="loginModel">
		<form class="form-signin">
			<h2 class="form-signin-heading">请登录</h2>
			<input type="text" class="form-control" v-model="${userNameFiled}"
				placeholder="用户名" required="" autofocus="" /> <input
				type="password" class="form-control" v-model="${userPwdFiled}"
				placeholder="密码" required="" /> <input type="checkbox"
				value="remember-me" id="rememberMe" v-model="rememberMe">
			记住我<br /> <br />
			<button class="btn btn-lg btn-primary btn-block" type="button" @click = "login()">登录</button>
		</form>
	</div>

<script th:src="@{/mystatic/jquery/jquery.min.js}"></script>
<script th:src="@{/mystatic/js/jqAlert.js}"></script>
<script th:src="@{/mystatic/js/ajaxFactory.js}"></script>
<script th:src="@{/mystatic/vue/vue.min.js}"></script>
<script th:src="@{/mystatic/js/config.js}"></script>

<script>

	var user = localStorage.getItem("user");

	var userName = '';
	var password = '';
	
	if (user != null && user != undefined) {
        try {
            user = JSON.parse(user);
            userName = user.userName;
            password = user.password;
        } catch(err){}
	} 

	var loginVue = new Vue({
		el : "#loginModel",
		data : {
			${userNameFiled} : userName,
			${userPwdFiled} : password,
			rememberMe : ''
		},
		methods : {
			login : function() {
				var that = this;
				$z.ajaxStrAndJson({
					url : basePath + "/login/doLogin",
					data : {
						${userNameFiled} : that.${userNameFiled},
						${userPwdFiled} : that.${userPwdFiled}
					},
					success : function(data) {
						$z.dealCommonResult(data, function() {
							//如果点了记住我，则存储到本地
							if (that.rememberMe) {
								localStorage.setItem("user", JSON.stringify({
									userName : that.${userNameFiled},
									password : that.${userPwdFiled}
								}));
							}
							window.location.href = basePath + '/home';
						});

						

					}
				});
			}
		}
	});
</script>


</body>

</html>
</#if>
</#if>


<#if theme == "前后端分离响应式">
<#if jsFrameWork == "jquery">
<!DOCTYPE html>
<html>

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">


		<title> - 登录</title>
		<meta name="keywords" content="">
		<meta name="description" content="">

		<link rel="shortcut icon" href="favicon.ico">
		<link href="../css/bootstrap.min-${clientStyleVal}.css" rel="stylesheet">
		<link href="../css/font-awesome.css?v=4.4.0" rel="stylesheet">

		<link href="../css/animate.css" rel="stylesheet">
		<link href="../css/style.css?v=4.1.0" rel="stylesheet">
		<link href="../css/plugins/progressbar/mprogress.css" rel="stylesheet" />
		<link href="../css/plugins/progressbar/style.css" rel="stylesheet" />
		<!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
		<script>if(window.top !== window.self){ window.top.location = window.location;}</script>
	</head>

	<body class="gray-bg">

		<div class="middle-box text-center loginscreen  animated fadeInDown" id="loginModel">
			<div>
				<div>

					<!-- <h1 class="logo-name">h</h1> -->

				</div>
				<h3>欢迎使用 codeMan</h3>

				<div class="form-group">
					<input type="text" class="form-control" placeholder="用户名" required="" id='userName' />
				</div>
				<div class="form-group">
					<input type="password" class="form-control" placeholder="密码" required="" id="password" />
				</div>
				<div class="form-group">
					<input type="checkbox" value="remember-me" id="rememberMe" id="rememberMe" />
					记住我
				</div>
				<button class="btn btn-primary block full-width m-b" id="loginBT">登 录</button>

				<!-- <p class="text-muted text-center"> <a href="login.html#"><small>忘记密码了？</small></a> | <a href="register.html">注册一个新账号</a>
				</p> -->

			</div>
		</div>

		<!-- 全局js -->
		<script src="../js/config/config.js"></script>
		<script src="../js/jquery.min.js?v=2.1.4"></script>
		<script src="../js/bootstrap.min.js?v=3.3.6"></script>
		<script src="../js/vue/vue.min.js"></script>
		<script src="../js/util/ajaxFactory.js"></script>
		<script src="../js/plugins/layer/layer.min.js"></script>
		<script src="../js/plugins/progressbar/mprogress.js"></script>
		<script src="../js/plugins/progressbar/init-mprogress.js"></script>
		<script>

			var user = localStorage.getItem("user");

			if (user != null && user != undefined) {
				try {
					user = JSON.parse(user);
					$("#userName").val(user.userName);
					$("#password").val(user.password);
				} catch (err) {}
			}

			$("#loginBT").on("click", function() {

				$z.ajaxStrAndJson({
					url: basePath + "/login/doLogin",
					data: {
						${userNameFiled}: $("#userName").val(),
						${userPwdFiled}: $("#password").val()
					},
					success: function(data) {
						$z.dealCommonResult(data, function() {
							var rememberMe = $("#rememberMe").prop("checked");
							//如果点了记住我，则存储到本地
							if (rememberMe) {
								localStorage.setItem("user", JSON.stringify({
									userName: $("#userName").val(),
									password: $("#password").val()
								}));
							}
							window.location.href = "home.html";
						});

						

					}
				});
			});
		</script>
	</body>
</html>
</#if>

<#if jsFrameWork == "vue">
<!DOCTYPE html>
<html>

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">


		<title> - 登录</title>
		<meta name="keywords" content="">
		<meta name="description" content="">

		<link rel="shortcut icon" href="favicon.ico">
		<link href="../css/bootstrap.min-${clientStyleVal}.css" rel="stylesheet">
		<link href="../css/font-awesome.css?v=4.4.0" rel="stylesheet">

		<link href="../css/animate.css" rel="stylesheet">
		<link href="../css/style.css?v=4.1.0" rel="stylesheet">
		<link href="../css/plugins/progressbar/mprogress.css" rel="stylesheet" />
		<link href="../css/plugins/progressbar/style.css" rel="stylesheet" />
		<!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
		<script>if(window.top !== window.self){ window.top.location = window.location;}</script>
	</head>

	<body class="gray-bg">

		<div class="middle-box text-center loginscreen  animated fadeInDown" id="loginModel">
			<div>
				<div>

					<!-- <h1 class="logo-name">h</h1> -->

				</div>
				<h3>欢迎使用 codeMan</h3>

				<div class="form-group">
					<input type="text" class="form-control" placeholder="用户名" required="" v-model="${userNameFiled}" />
				</div>
				<div class="form-group">
					<input type="password" class="form-control" placeholder="密码" required="" v-model="${userPwdFiled}" />
				</div>
				<div class="form-group">
					<input type="checkbox" value="remember-me" id="rememberMe" v-model="rememberMe" />
					记住我
				</div>
				<button class="btn btn-primary block full-width m-b" @click="login()">登 录</button>

				<!-- <p class="text-muted text-center"> <a href="login.html#"><small>忘记密码了？</small></a> | <a href="register.html">注册一个新账号</a>
				</p> -->

			</div>
		</div>

		<!-- 全局js -->
		<script src="../js/config/config.js"></script>
		<script src="../js/jquery.min.js?v=2.1.4"></script>
		<script src="../js/bootstrap.min.js?v=3.3.6"></script>
		<script src="../js/vue/vue.min.js"></script>
		<script src="../js/util/ajaxFactory.js"></script>
		<script src="../js/plugins/layer/layer.min.js"></script>
		<script src="../js/plugins/progressbar/mprogress.js"></script>
		<script src="../js/plugins/progressbar/init-mprogress.js"></script>
		<script>

			var user = localStorage.getItem("user");

			var userName = '';
			var password = '';

			if (user != null && user != undefined) {
				try {
					user = JSON.parse(user);
					userName = user.userName;
					password = user.password;
				} catch (err) {}
			}

			var loginVue = new Vue({
				el: "#loginModel",
				data: {
					${userNameFiled}: userName,
					${userPwdFiled}: password,
					rememberMe: ''
				},
				methods: {
					login: function() {
						var that = this;
						$z.ajaxStrAndJson({
							url: basePath + "/login/doLogin",
							data: {
								${userNameFiled}: that.${userNameFiled},
								${userPwdFiled}: that.${userPwdFiled}
							},
							success: function(data) {
								$z.dealCommonResult(data, function() {
									//如果点了记住我，则存储到本地
									if (that.rememberMe) {
										localStorage.setItem("user", JSON.stringify({
											userName: that.${userNameFiled},
											password: that.${userPwdFiled}
										}));
									}
									window.location.href = "home.html";
								});

								

							}
						});
					}
				}
			});
		</script>

	</body>

</html>
</#if>
</#if>


