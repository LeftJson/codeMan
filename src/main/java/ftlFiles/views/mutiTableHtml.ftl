<#if theme == "经典后台Thymleaf版">
<#if jsFrameWork == "jquery">
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>${projectName}</title>
<link rel="stylesheet" th:href="@{/mystatic/bootstrap/css/bootstrap.min-${clientStyleVal}.css}">
<link rel="stylesheet" th:href="@{/mystatic/bootstrap/css/bootstrap-table.css}">
<link rel="stylesheet" th:href="@{/mystatic/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css}">
<link rel="stylesheet" th:href="@{/mystatic/multiselect/css/bootstrap-multiselect.css}">
<link rel="stylesheet" th:href="@{/mystatic/css/admin.css}">
<link rel="stylesheet" th:href="@{/mystatic/css/ui.css}">
<link rel="stylesheet" th:href="@{/mystatic/css/ui2.css}">
<link rel="stylesheet" th:href="@{/mystatic/progressbar/css/mprogress.css}">
<link rel="stylesheet" th:href="@{/mystatic/progressbar/css/style.css}">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script th:src="@{/mystatic/html5shiv/html5shiv.min.js}"></script>
    <script th:src="@{/mystatic/respond/respond.min.js}"></script>
  <![endif]-->
<style>
/* 外面盒子样式---自己定义 */
.page_div {
	margin: 20px 10px 20px 0;
	color: #666
}
/* 页数按钮样式 */
.page_div button {
	display: inline-block;
	min-width: 30px;
	height: 28px;
	cursor: pointer;
	color: #666;
	font-size: 13px;
	line-height: 28px;
	background-color: #f9f9f9;
	border: 1px solid #dce0e0;
	text-align: center;
	margin: 0 4px;
	-webkit-appearance: none;
	-moz-appearance: none;
	appearance: none;
}

#firstPage, #lastPage, #nextPage, #prePage {
	width: 50px;
	color: #0073A9;
	border: 1px solid #0073A9
}

#nextPage, #prePage {
	width: 70px
}

.page_div .current {
	background-color: #0073A9;
	border-color: #0073A9;
	color: #FFF
}
/* 页面数量 */
.totalPages {
	margin: 0 10px
}

.totalPages span, .totalSize span {
	color: #0073A9;
	margin: 0 5px
}
/*button禁用*/
.page_div button:disabled {
	opacity: .5;
	cursor: no-drop
}
</style>
</head>

<body class="admin-body toggle-nav fs">
	<div class="container-fluid">
		<div class="row" style="padding-left: 0">
			<div class="main" style="padding-top: 0">
				<div class="container-fluid">
					<div class="admin-new-box ui-admin-content">
						<!--后台正文区域-->
						<div class="ui-panel">
							<div class="ui-title-bar">
								<div class="ui-title">${currentMutiMethodCn}管理模块</div>
							</div>
							<!-- Tab panes -->
							<div class="tab-content">
								<div role="tabpanel" class="tab-pane active" id="uiTab01">
									<!-- 查询区域 -->
									<div>
										<#list tableConditionModels as data>
											<#if data.serviceType == "字符串" && data.unchangeValue =="">
												<#if data.compareText==">= && <=">
												<input type="text" id="${data.anotherTableName}${data.filed_eng}-startQuery" placeholder="开始" style="margin-top: 10px"/>——
												<input type="text" id="${data.anotherTableName}${data.filed_eng}-endQuery" placeholder="结束" style="margin-top: 10px"/>&nbsp;
												<#else>
												<input type="text" id="${data.anotherTableName}${data.filed_eng}-query" placeholder="${data.filed_cn}" style="margin-top: 10px"/>&nbsp;
												</#if>
											</#if>
											<#if data.serviceType == "数字" && data.unchangeValue =="">
												<#if data.compareText==">= && <=">
												<input type="number" id="${data.anotherTableName}${data.filed_eng}-startQuery" placeholder="开始" style="margin-top: 10px"/>——
												<input type="number" id="${data.anotherTableName}${data.filed_eng}-endQuery" placeholder="结束" style="margin-top: 10px"/>&nbsp;
												<#else>
												<input type="number" id="${data.anotherTableName}${data.filed_eng}-query" placeholder="${data.filed_cn}" style="margin-top: 10px"/>&nbsp;
												</#if>
											</#if>
											<#if (data.serviceType == "布尔" || data.serviceType == "状态码") && data.unchangeValue =="">
												${data.filed_cn}：
												<select id="${data.anotherTableName}${data.filed_eng}-query" style="margin-top: 10px; height: 28px">
													<option value="">--请选择--</option>
													<#list data.serviceText?keys as key>
													<option value="${data.serviceText["${key}"]}">${key}</option>
													</#list>
												</select>&nbsp;
											</#if>
											<#if data.serviceType == "日期" && data.unchangeValue ==""> 
												${data.filed_cn}：
												<#if data.compareText==">= && <=">
												<input type="date" id="${data.anotherTableName}${data.filed_eng}-startQuery" placeholder="开始日期" style="margin-top: 10px"/>——
												<input type="date" id="${data.anotherTableName}${data.filed_eng}-endQuery" placeholder="结束日期" style="margin-top: 10px"/>&nbsp;
												<#else>
												<input type="date" id="${data.anotherTableName}${data.filed_eng}-query" placeholder="${data.filed_cn}" style="margin-top: 10px"/>&nbsp;
												</#if>
											</#if>
											<#if data.serviceType == "文字域" && data.unchangeValue =="">
												<#if data.compareText==">= && <=">
												<input type="text" id="${data.anotherTableName}${data.filed_eng}-startQuery" placeholder="开始" style="margin-top: 10px"/>——
												<input type="text" id="${data.anotherTableName}${data.filed_eng}-endQuery" placeholder="结束" style="margin-top: 10px"/>&nbsp;
												<#else>
												<input type="text" id="${data.anotherTableName}${data.filed_eng}-query" placeholder="${data.filed_cn}" style="margin-top: 10px"/>&nbsp;
												</#if>
											</#if>
										</#list>
										<button type="button" class="btn btn-primary btn-sm" onclick="queryInfo()">查询</button>&nbsp;
										<button type="button" class="btn btn-primary btn-sm" onclick="exportExcel()">导出excel</button>
									</div>
                                    <br/>
									<!-- 查询区域 end-->
									
									<!-- 查询结果表格显示区域 -->
									<div class="table-responsive" style="overflow: scroll;">
										<table id="newsContent" class="table table-hover table-bordered text-nowrap">
											<tr>
												<#list tableFiledModels as data>
													<#if data.canSort == "是">
														<th>${data.filedText_cn}<a href='#' onclick='$crud.setAscColumn(this,"${data.anotherTableName}.${data.filedText_eng}")'>↑</a>&nbsp;<a href='#' onclick='$crud.setDescColumn(this,"${data.anotherTableName}.${data.filedText_eng}")'>↓</a></th>
													</#if>
													<#if data.canSort == "否">
														<th>${data.filedText_cn}</th>
													</#if>
												</#list>
											</tr>
											<tbody id="dataTable">
												
											</tbody>
										</table>
										<div id="pageID" class="page_div"></div>
									</div>
									<!-- 查询结果表格显示区域 end-->
								</div>
							</div>
						</div>
						<!--后台正文区域结束-->

					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	<!-- 表格内容模板  -->
	<script id="tableContentTemplate" type="text/html">
		{{#result}}
			<tr>
				<#list tableFiledModels as data>
					<td>{{${data.anotherFiledName}}}</td>
				</#list>
			</tr>
		{{/result}}
	</script>

	<script th:src="@{/mystatic/jquery/jquery.min.js}"></script>
	<script th:src="@{/mystatic/js/pageMe.js}"></script>
	<script th:src="@{/mystatic/bootstrap/js/bootstrap.min.js}"></script>
	<script th:src="@{/mystatic/bootstrap/js/bootstrap-table.js}"></script>
	<script th:src="@{/mystatic/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js}"></script>
	<script th:src="@{/mystatic/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js}"></script>
	<script th:src="@{/mystatic/multiselect/js/bootstrap-multiselect.js}"></script>
	<script th:src="@{/mystatic/js/admin.js}"></script>
	<script th:src="@{/mystatic/js/ajaxFactory.js}"></script>
	<script th:src="@{/mystatic/js/crudFactory.js}"></script>
    <script th:src="@{/mystatic/js/jqAlert.js}"></script>
	<script th:src="@{/mystatic/echarts/echarts.min.js}"></script>
	<!-- 进度条  -->
	<script th:src="@{/mystatic/progressbar/js/mprogress.js}"></script>
	<script th:src="@{/mystatic/progressbar/js/init-mprogress.js}"></script>
	<script th:src="@{/mystatic/js/mustache/mustache.min.js}"></script>
	<script th:src="@{/mystatic/js/config.js}"></script>

	<script>
		
		var currentPage = 1;
		var totalPage;
		var sqlMap = {};
		//排序的数据
		var orderData = [];
		var controllerPrefix = "${currentMutiEng}Muti";
		var methodName = "${currentMutiMethodEng}";
		
		$(function() {

			//后面可以根据自身业务具体添加查询条件，目前条件只有当前页
			
			//crudFactory.js
			$crud.getDataByCurrentPage();

		});

		function queryInfo() {
			sqlMap = {};
			<#list tableConditionModels as data>
			<#if data.unchangeValue=="">
			<#if data.compareText==">= && <=">
			sqlMap.start${data.anotherTableName}_${data.filed_eng}=$("#${data.anotherTableName}${data.filed_eng}-startQuery").val();
			sqlMap.end${data.anotherTableName}_${data.filed_eng}=$("#${data.anotherTableName}${data.filed_eng}-endQuery").val();
			<#else>
			sqlMap.${data.anotherTableName}_${data.filed_eng}=$("#${data.anotherTableName}${data.filed_eng}-query").val();
			</#if>
			</#if>
			</#list>
			currentPage = 1;
			orderData = [];
			$crud.getDataByCurrentPage();
		}

		function makeResult(data) {
			for (var i = 0; i < data.length; i++) {
				if (data[i] == null) {
					data[i] = {};
					<#list tableFiledModels as data>
					data[i]["${data.anotherFiledName}"] = "无";
					</#list>
				}
				<#list tableFiledModels as data>
				<#if data.filedType == "布尔" || data.filedType == "状态码">
					<#list data.filedComment?keys as key>
					if(data[i]["${data.anotherFiledName}"] == "${data.filedComment["${key}"]}") {
						data[i]["${data.anotherFiledName}"] = "${key}";
					}
					</#list>
				</#if>
				</#list>
			}
		}

		function exportExcel() {

			//显示进度条
			InitMprogress();

            var param = '';

            for (var key in sqlMap) {
                param += key + "=" + sqlMap[key] + "&";
            }

			window.location.href = basePath + "/${currentMutiEng}Muti/${currentMutiMethodEng}ExportExcel?" + param;
			// 进度条消失
			setTimeout("MprogressEnd()", totalPage / 20 * 1000);
		}
	</script>
</body>

</html>
</#if>

<#if jsFrameWork == "vue">
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>${projectName}</title>
<link rel="stylesheet" th:href="@{/mystatic/bootstrap/css/bootstrap.min-${clientStyleVal}.css}">
<link rel="stylesheet" th:href="@{/mystatic/bootstrap/css/bootstrap-table.css}">
<link rel="stylesheet" th:href="@{/mystatic/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css}">
<link rel="stylesheet" th:href="@{/mystatic/multiselect/css/bootstrap-multiselect.css}">
<link rel="stylesheet" th:href="@{/mystatic/css/admin.css}">
<link rel="stylesheet" th:href="@{/mystatic/css/ui.css}">
<link rel="stylesheet" th:href="@{/mystatic/css/ui2.css}">
<link rel="stylesheet" th:href="@{/mystatic/progressbar/css/mprogress.css}">
<link rel="stylesheet" th:href="@{/mystatic/progressbar/css/style.css}">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script th:src="@{/mystatic/html5shiv/html5shiv.min.js}"></script>
    <script th:src="@{/mystatic/respond/respond.min.js}"></script>
  <![endif]-->
<style>
/* 外面盒子样式---自己定义 */
.page_div {
	margin: 20px 10px 20px 0;
	color: #666
}
/* 页数按钮样式 */
.page_div button {
	display: inline-block;
	min-width: 30px;
	height: 28px;
	cursor: pointer;
	color: #666;
	font-size: 13px;
	line-height: 28px;
	background-color: #f9f9f9;
	border: 1px solid #dce0e0;
	text-align: center;
	margin: 0 4px;
	-webkit-appearance: none;
	-moz-appearance: none;
	appearance: none;
}

#firstPage, #lastPage, #nextPage, #prePage {
	width: 50px;
	color: #0073A9;
	border: 1px solid #0073A9
}

#nextPage, #prePage {
	width: 70px
}

.page_div .current {
	background-color: #0073A9;
	border-color: #0073A9;
	color: #FFF
}
/* 页面数量 */
.totalPages {
	margin: 0 10px
}

.totalPages span, .totalSize span {
	color: #0073A9;
	margin: 0 5px
}
/*button禁用*/
.page_div button:disabled {
	opacity: .5;
	cursor: no-drop
}
/*防止vue页面闪烁*/
[v-cloak] {
	display: none !important;
}
</style>
</head>

<body class="admin-body toggle-nav fs">
	<div class="container-fluid">
		<div class="row" style="padding-left: 0">
			<div class="main" style="padding-top: 0">
				<div class="container-fluid">
					<div class="admin-new-box ui-admin-content">
						<!--后台正文区域-->
						<div class="ui-panel">
							<div class="ui-title-bar">
								<div class="ui-title">${currentMutiMethodCn}管理模块</div>
							</div>
							<!-- Tab panes -->
							<div class="tab-content">
								<div role="tabpanel" class="tab-pane active" id="uiTab01">
									<!-- 查询区域 -->
									<div id="queryModel">
										<#list tableConditionModels as data>
											<#if data.serviceType == "字符串" && data.unchangeValue =="">
												<#if data.compareText==">= && <=">
												<input type="text" v-model="start${data.anotherTableName}_${data.filed_eng}" placeholder="开始" style="margin-top: 10px"/>——
												<input type="text" v-model="end${data.anotherTableName}_${data.filed_eng}" placeholder="结束" style="margin-top: 10px"/>&nbsp;
												<#else>
												<input type="text" v-model="${data.anotherTableName}_${data.filed_eng}" placeholder="${data.filed_cn}" style="margin-top: 10px"/>&nbsp;
												</#if>
											</#if>
											<#if data.serviceType == "数字" && data.unchangeValue =="">
												<#if data.compareText==">= && <=">
												<input type="number" v-model="start${data.anotherTableName}_${data.filed_eng}" placeholder="开始" style="margin-top: 10px"/>——
												<input type="number" v-model="end${data.anotherTableName}_${data.filed_eng}" placeholder="结束" style="margin-top: 10px"/>&nbsp;
												<#else>
												<input type="number"  v-model="${data.anotherTableName}_${data.filed_eng}" placeholder="${data.filed_cn}" style="margin-top: 10px"/>&nbsp;
												</#if>
											</#if>
											<#if (data.serviceType == "布尔" || data.serviceType == "状态码") && data.unchangeValue =="">
												${data.filed_cn}：
												<select v-model="${data.anotherTableName}_${data.filed_eng}" style="margin-top: 10px; height: 28px">
													<option value="">--请选择--</option>
													<#list data.serviceText?keys as key>
													<option value="${data.serviceText["${key}"]}">${key}</option>
													</#list>
												</select>&nbsp;
											</#if>
											<#if data.serviceType == "日期" && data.unchangeValue ==""> 
												${data.filed_cn}：
												<#if data.compareText==">= && <=">
												<input type="date" v-model="start${data.anotherTableName}_${data.filed_eng}" placeholder="开始日期" style="margin-top: 10px"/>——
												<input type="date" v-model="end${data.anotherTableName}_${data.filed_eng}" placeholder="结束日期" style="margin-top: 10px"/>&nbsp;
												<#else>
												<input type="date" v-model="${data.anotherTableName}_${data.filed_eng}" placeholder="${data.filed_cn}" style="margin-top: 10px"/>&nbsp;
												</#if>
											</#if>
											<#if data.serviceType == "文字域" && data.unchangeValue =="">
												<#if data.compareText==">= && <=">
												<input type="text" v-model="start${data.anotherTableName}_${data.filed_eng}" placeholder="开始" style="margin-top: 10px"/>——
												<input type="text" v-model="end${data.anotherTableName}_${data.filed_eng}" placeholder="结束" style="margin-top: 10px"/>&nbsp;
												<#else>
												<input type="text" v-model="${data.anotherTableName}_${data.filed_eng}" placeholder="${data.filed_cn}" style="margin-top: 10px"/>&nbsp;
												</#if>
											</#if>
										</#list>
										<button type="button" class="btn btn-primary btn-sm" @click="queryInfo()">查询</button>&nbsp;
										<button type="button" class="btn btn-primary btn-sm" @click="exportExcel()">导出excel</button>
									</div>
                                    <br/>
									<!-- 查询区域 end-->
									
									<!-- 查询结果表格显示区域 -->
									<div id="newsContent" class="table-responsive" style="overflow: scroll;" v-cloak>
										<table class="table table-hover table-bordered text-nowrap">
											<tr>
												<#list tableFiledModels as data>
													<#if data.canSort == "是">
														<th>${data.filedText_cn}<a href='#' onclick='$crud.setAscColumn(this,"${data.anotherTableName}.${data.filedText_eng}")'>↑</a>&nbsp;<a href='#' onclick='$crud.setDescColumn(this,"${data.anotherTableName}.${data.filedText_eng}")'>↓</a></th>
													</#if>
													<#if data.canSort == "否">
														<th>${data.filedText_cn}</th>
													</#if>
												</#list>
											</tr>
											<tbody id="dataTable">
												<tr v-for="data in result">
												<#list tableFiledModels as data>
													<td>{{data.${data.anotherFiledName}}}</td>
												</#list>
												</tr>
											</tbody>
										</table>
										<div id="pageID" class="page_div"></div>
									</div>
									<!-- 查询结果表格显示区域 end-->
								</div>
							</div>
						</div>
						<!--后台正文区域结束-->

					</div>
				</div>
			</div>
		</div>
	</div>

	<script th:src="@{/mystatic/jquery/jquery.min.js}"></script>
	<script th:src="@{/mystatic/js/pageMe.js}"></script>
	<script th:src="@{/mystatic/bootstrap/js/bootstrap.min.js}"></script>
	<script th:src="@{/mystatic/bootstrap/js/bootstrap-table.js}"></script>
	<script th:src="@{/mystatic/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js}"></script>
	<script th:src="@{/mystatic/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js}"></script>
	<script th:src="@{/mystatic/multiselect/js/bootstrap-multiselect.js}"></script>
	<script th:src="@{/mystatic/js/admin.js}"></script>
	<script th:src="@{/mystatic/js/ajaxFactory.js}"></script>
	<script th:src="@{/mystatic/js/crudVueFactory.js}"></script>
    <script th:src="@{/mystatic/js/jqAlert.js}"></script>
	<script th:src="@{/mystatic/echarts/echarts.min.js}"></script>
	<script th:src="@{/mystatic/vue/vue.min.js}"></script>
	<!-- 进度条  -->
	<script th:src="@{/mystatic/progressbar/js/mprogress.js}"></script>
	<script th:src="@{/mystatic/progressbar/js/init-mprogress.js}"></script>
	<script th:src="@{/mystatic/js/config.js}"></script>

	<script>
		
		var currentPage = 1;
		var totalPage;
		var sqlMap = {};
		//排序的数据
		var orderData = [];
		var controllerPrefix = "${currentMutiEng}Muti";
		var methodName = "${currentMutiMethodEng}";
		
		var queryVue = new Vue({
			el : '#queryModel',
			data : {
		<#list tableConditionModels as data>
			<#if data.unchangeValue == "">
				<#if data.compareText==">= && <=">
				start${data.anotherTableName}_${data.filed_eng} : '',
				end${data.anotherTableName}_${data.filed_eng} : ''<#if data_has_next>,</#if>
				<#else>
				${data.anotherTableName}_${data.filed_eng} : ''<#if data_has_next>,</#if>
				</#if>
			</#if>
		</#list>
			},
			methods : {
				queryInfo : function() {
					sqlMap = {};
			<#list tableConditionModels as data>
				<#if data.unchangeValue == "">
					<#if data.compareText==">= && <=">
					sqlMap.start${data.anotherTableName}_${data.filed_eng} = this.start${data.anotherTableName}_${data.filed_eng};
					sqlMap.end${data.anotherTableName}_${data.filed_eng} = this.end${data.anotherTableName}_${data.filed_eng};
					<#else>
					sqlMap.${data.anotherTableName}_${data.filed_eng} = this.${data.anotherTableName}_${data.filed_eng};
					</#if>
				</#if>
			</#list>
					currentPage = 1;
					orderData = [];
					$crud.getDataByCurrentPage();
				},
				exportExcel : function() {

					//显示进度条
					InitMprogress();

                    var param = '';

                    for (var key in sqlMap) {
                        param += key + "=" + sqlMap[key] + "&";
                    }

                    window.location.href = basePath + "/${currentMutiEng}Muti/${currentMutiMethodEng}ExportExcel?" + param;

					// 进度条消失
					setTimeout("MprogressEnd()", totalPage / 20 * 1000);
				}
			}

		});
		
		
		function makeResult(data) {
			for (var i = 0; i < data.length; i++) {
				if (data[i] == null) {
					data[i] = {};
					<#list tableFiledModels as data>
					data[i]["${data.anotherFiledName}"] = "无";
					</#list>
				}
				<#list tableFiledModels as data>
				<#if data.filedType == "布尔" || data.filedType == "状态码">
					<#list data.filedComment?keys as key>
					if(data[i]["${data.anotherFiledName}"] == "${data.filedComment["${key}"]}") {
						data[i]["${data.anotherFiledName}"] = "${key}";
					}
					</#list>
				</#if>
				</#list>
			}
		}
		
		var tableVue = new Vue({
			el : '#newsContent',
			data : {
				result : []
			},
			//created:
			mounted : function() {
				$crud.getDataByCurrentPage();
			}
		});
		
		//=======================================================vue 2.0
		
	</script>
</body>

</html>
</#if>
</#if>


<#if theme == "前后端分离响应式">
<#if jsFrameWork == "jquery">
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>${projectName}</title>
		<meta name="keywords" content="">
		<meta name="description" content="">
		<link rel="shortcut icon" href="../favicon.ico">
		<link href="../../css/bootstrap.min-${clientStyleVal}.css" rel="stylesheet">
		<link href="../../css/font-awesome.css?v=4.4.0" rel="stylesheet">
		<link href="../../css/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
		<link href="../../css/animate.css" rel="stylesheet">
		<link href="../../css/style.css?v=4.1.0" rel="stylesheet">
		<link href="../../css/plugins/pageMe/pageMe.css" rel="stylesheet" />
		<link href="../../css/plugins/progressbar/mprogress.css" rel="stylesheet" />
		<link href="../../css/plugins/progressbar/style.css" rel="stylesheet" />
	</head>

	<body class="gray-bg">
		<div class="wrapper wrapper-content animated fadeInRight">

			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>${currentMutiMethodCn}管理模块</h5>
				</div>
				<div class="ibox-content">
					<div class="row row-lg">
						<div class="clearfix hidden-xs"></div>
						<div class="col-sm-12">
							<!-- Example Checkbox Select -->
							<div class="example-wrap">

								<!-- 查询区域 -->
								<div>
									<#list tableConditionModels as data>
										<#if data.serviceType == "字符串" && data.unchangeValue =="">
											<#if data.compareText==">= && <=">
											<input type="text" id="${data.anotherTableName}${data.filed_eng}-startQuery" placeholder="开始" style="margin-top: 10px"/>——
											<input type="text" id="${data.anotherTableName}${data.filed_eng}-endQuery" placeholder="结束" style="margin-top: 10px"/>&nbsp;
											<#else>
											<input type="text" id="${data.anotherTableName}${data.filed_eng}-query" placeholder="${data.filed_cn}" style="margin-top: 10px"/>&nbsp;
											</#if>
										</#if>
										<#if data.serviceType == "数字" && data.unchangeValue =="">
											<#if data.compareText==">= && <=">
											<input type="number" id="${data.anotherTableName}${data.filed_eng}-startQuery" placeholder="开始" style="margin-top: 10px"/>——
											<input type="number" id="${data.anotherTableName}${data.filed_eng}-endQuery" placeholder="结束" style="margin-top: 10px"/>&nbsp;
											<#else>
											<input type="number" id="${data.anotherTableName}${data.filed_eng}-query" placeholder="${data.filed_cn}" style="margin-top: 10px"/>&nbsp;
											</#if>
										</#if>
										<#if (data.serviceType == "布尔" || data.serviceType == "状态码") && data.unchangeValue =="">
											${data.filed_cn}：
											<select id="${data.anotherTableName}${data.filed_eng}-query" style="margin-top: 10px; height: 28px">
												<option value="">--请选择--</option>
												<#list data.serviceText?keys as key>
												<option value="${data.serviceText["${key}"]}">${key}</option>
												</#list>
											</select>&nbsp;
										</#if>
										<#if data.serviceType == "日期" && data.unchangeValue ==""> 
											${data.filed_cn}：
											<#if data.compareText==">= && <=">
											<input type="date" id="${data.anotherTableName}${data.filed_eng}-startQuery" placeholder="开始日期" style="margin-top: 10px"/>——
											<input type="date" id="${data.anotherTableName}${data.filed_eng}-endQuery" placeholder="结束日期" style="margin-top: 10px"/>&nbsp;
											<#else>
											<input type="date" id="${data.anotherTableName}${data.filed_eng}-query" placeholder="${data.filed_cn}" style="margin-top: 10px"/>&nbsp;
											</#if>
										</#if>
										<#if data.serviceType == "文字域" && data.unchangeValue =="">
											<#if data.compareText==">= && <=">
											<input type="text" id="${data.anotherTableName}${data.filed_eng}-startQuery" placeholder="开始" style="margin-top: 10px"/>——
											<input type="text" id="${data.anotherTableName}${data.filed_eng}-endQuery" placeholder="结束" style="margin-top: 10px"/>&nbsp;
											<#else>
											<input type="text" id="${data.anotherTableName}${data.filed_eng}-query" placeholder="${data.filed_cn}" style="margin-top: 10px"/>&nbsp;
											</#if>
										</#if>
									</#list>
									<button type="button" class="btn btn-primary btn-sm" onclick="queryInfo()">查询</button>&nbsp;
									<button type="button" class="btn btn-primary btn-sm" onclick="exportExcel()">导出excel</button>
								</div>
                                <br/>
								<!-- 查询区域 end-->

								<!-- 查询结果表格显示区域 -->
								<div class="table-responsive" style="overflow: scroll;">
									<table id="newsContent" class="table table-hover table-bordered text-nowrap">
										<tr>
											<#list tableFiledModels as data>
												<#if data.canSort == "是">
													<th>${data.filedText_cn}<a href='#' onclick='$crud.setAscColumn(this,"${data.anotherTableName}.${data.filedText_eng}")'>↑</a>&nbsp;<a href='#' onclick='$crud.setDescColumn(this,"${data.anotherTableName}.${data.filedText_eng}")'>↓</a></th>
												</#if>
												<#if data.canSort == "否">
													<th>${data.filedText_cn}</th>
												</#if>
											</#list>
										</tr>
										<tbody id="dataTable">
											
										</tbody>
									</table>
									<div id="pageID" class="page_div"></div>
								</div>
								<!-- 查询结果表格显示区域 end-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- 表格内容模板  -->
		<script id="tableContentTemplate" type="text/html">
			{{#result}}
				<tr>
					<#list tableFiledModels as data>
						<td>{{${data.anotherFiledName}}}</td>
					</#list>
				</tr>
			{{/result}}
		</script>


		<!-- 全局js -->
		<script src="../../js/jquery.min.js?v=2.1.4"></script>
		<script src="../../js/bootstrap.min.js?v=3.3.6"></script>

		<!-- 自定义js -->
		<script src="../../js/content.js?v=1.0.0"></script>


		<!-- Bootstrap table -->
		<script src="../../js/plugins/bootstrap-table/bootstrap-table.min.js"></script>
		<script src="../../js/plugins/bootstrap-table/bootstrap-table-mobile.min.js"></script>
		<script src="../../js/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>

		<!-- Peity -->
		<!-- <script src="../../js/demo/bootstrap-table-demo.js"></script> -->
		<script src="../../js/util/ajaxFactory.js"></script>
		<script src="../../js/util/crudFactory.js"></script>
		<script src="../../js/util/pageMe.js"></script>
		<script src="../../js/config/config.js"></script>
		<script src="../../js/plugins/layer/layer.min.js"></script>
		<script src="../../js/plugins/progressbar/init-mprogress.js"></script>
		<script src="../../js/plugins/progressbar/mprogress.js"></script>
		<script src="../../js/plugins/mustache/mustache.js"></script>

		<script>
			var currentPage = 1;
			var totalPage;
			var sqlMap = {};
			//排序的数据
			var orderData = [];
			var controllerPrefix = "${currentMutiEng}Muti";
			var methodName = "${currentMutiMethodEng}";
			
			$(function() {
	
				//后面可以根据自身业务具体添加查询条件，目前条件只有当前页
				
				//crudFactory.js
				$crud.getDataByCurrentPage();
	
			});
	
			function queryInfo() {
				sqlMap = {};
				<#list tableConditionModels as data>
				<#if data.unchangeValue=="">
				<#if data.compareText==">= && <=">
				sqlMap.start${data.anotherTableName}_${data.filed_eng}=$("#${data.anotherTableName}${data.filed_eng}-startQuery").val();
				sqlMap.end${data.anotherTableName}_${data.filed_eng}=$("#${data.anotherTableName}${data.filed_eng}-endQuery").val();
				<#else>
				sqlMap.${data.anotherTableName}_${data.filed_eng}=$("#${data.anotherTableName}${data.filed_eng}-query").val();
				</#if>
				</#if>
				</#list>
				currentPage = 1;
				orderData = [];
				$crud.getDataByCurrentPage();
			}
	
			function makeResult(data) {
				for (var i = 0; i < data.length; i++) {
					if (data[i] == null) {
						data[i] = {};
						<#list tableFiledModels as data>
						data[i]["${data.anotherFiledName}"] = "无";
						</#list>
					}
					<#list tableFiledModels as data>
					<#if data.filedType == "布尔" || data.filedType == "状态码">
						<#list data.filedComment?keys as key>
						if(data[i]["${data.anotherFiledName}"] == "${data.filedComment["${key}"]}") {
							data[i]["${data.anotherFiledName}"] = "${key}";
						}
						</#list>
					</#if>
					</#list>
				}
			}
	
			function exportExcel() {
	
				//显示进度条
				InitMprogress();
	
	            var param = '';
	
	            for (var key in sqlMap) {
	                param += key + "=" + sqlMap[key] + "&";
	            }
	
				window.location.href = basePath + "/${currentMutiEng}Muti/${currentMutiMethodEng}ExportExcel?" + param;
				// 进度条消失
				setTimeout("MprogressEnd()", totalPage / 20 * 1000);
			}
		</script>

	</body>

</html>
</#if>

<#if jsFrameWork == "vue">
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>${projectName}</title>
		<meta name="keywords" content="">
		<meta name="description" content="">
		<link rel="shortcut icon" href="../favicon.ico">
		<link href="../../css/bootstrap.min-${clientStyleVal}.css" rel="stylesheet">
		<link href="../../css/font-awesome.css?v=4.4.0" rel="stylesheet">
		<link href="../../css/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
		<link href="../../css/animate.css" rel="stylesheet">
		<link href="../../css/style.css?v=4.1.0" rel="stylesheet">
		<link href="../../css/plugins/pageMe/pageMe.css" rel="stylesheet" />
		<link href="../../css/plugins/progressbar/mprogress.css" rel="stylesheet" />
		<link href="../../css/plugins/progressbar/style.css" rel="stylesheet" />
	</head>

	<body class="gray-bg">
		<div class="wrapper wrapper-content animated fadeInRight">

			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>${currentMutiMethodCn}管理模块</h5>
				</div>
				<div class="ibox-content">
					<div class="row row-lg">
						<div class="clearfix hidden-xs"></div>
						<div class="col-sm-12">
							<!-- Example Checkbox Select -->
							<div class="example-wrap">

								<!-- 查询区域 -->
								<div id="queryModel">
									<#list tableConditionModels as data>
										<#if data.serviceType == "字符串" && data.unchangeValue =="">
											<#if data.compareText==">= && <=">
											<input type="text" v-model="start${data.anotherTableName}_${data.filed_eng}" placeholder="开始" style="margin-top: 10px"/>——
											<input type="text" v-model="end${data.anotherTableName}_${data.filed_eng}" placeholder="结束" style="margin-top: 10px"/>&nbsp;
											<#else>
											<input type="text" v-model="${data.anotherTableName}_${data.filed_eng}" placeholder="${data.filed_cn}" style="margin-top: 10px"/>&nbsp;
											</#if>
										</#if>
										<#if data.serviceType == "数字" && data.unchangeValue =="">
											<#if data.compareText==">= && <=">
											<input type="number" v-model="start${data.anotherTableName}_${data.filed_eng}" placeholder="开始" style="margin-top: 10px"/>——
											<input type="number" v-model="end${data.anotherTableName}_${data.filed_eng}" placeholder="结束" style="margin-top: 10px"/>&nbsp;
											<#else>
											<input type="number"  v-model="${data.anotherTableName}_${data.filed_eng}" placeholder="${data.filed_cn}" style="margin-top: 10px"/>&nbsp;
											</#if>
										</#if>
										<#if (data.serviceType == "布尔" || data.serviceType == "状态码") && data.unchangeValue =="">
											${data.filed_cn}：
											<select v-model="${data.anotherTableName}_${data.filed_eng}" style="margin-top: 10px; height: 28px">
												<option value="">--请选择--</option>
												<#list data.serviceText?keys as key>
												<option value="${data.serviceText["${key}"]}">${key}</option>
												</#list>
											</select>&nbsp;
										</#if>
										<#if data.serviceType == "日期" && data.unchangeValue ==""> 
											${data.filed_cn}：
											<#if data.compareText==">= && <=">
											<input type="date" v-model="start${data.anotherTableName}_${data.filed_eng}" placeholder="开始日期" style="margin-top: 10px"/>——
											<input type="date" v-model="end${data.anotherTableName}_${data.filed_eng}" placeholder="结束日期" style="margin-top: 10px"/>&nbsp;
											<#else>
											<input type="date" v-model="${data.anotherTableName}_${data.filed_eng}" placeholder="${data.filed_cn}" style="margin-top: 10px"/>&nbsp;
											</#if>
										</#if>
										<#if data.serviceType == "文字域" && data.unchangeValue =="">
											<#if data.compareText==">= && <=">
											<input type="text" v-model="start${data.anotherTableName}_${data.filed_eng}" placeholder="开始" style="margin-top: 10px"/>——
											<input type="text" v-model="end${data.anotherTableName}_${data.filed_eng}" placeholder="结束" style="margin-top: 10px"/>&nbsp;
											<#else>
											<input type="text" v-model="${data.anotherTableName}_${data.filed_eng}" placeholder="${data.filed_cn}" style="margin-top: 10px"/>&nbsp;
											</#if>
										</#if>
									</#list>
									<button type="button" class="btn btn-primary btn-sm" @click="queryInfo()">查询</button>&nbsp;
									<button type="button" class="btn btn-primary btn-sm" @click="exportExcel()">导出excel</button>
								</div>
                                <br/>
								<!-- 查询区域 end-->

								<!-- 查询结果表格显示区域 -->
								<div id="newsContent" class="table-responsive" style="overflow: scroll;" v-cloak>
									<table class="table table-hover table-bordered text-nowrap">
										<tr>
											<#list tableFiledModels as data>
												<#if data.canSort == "是">
													<th>${data.filedText_cn}<a href='#' onclick='$crud.setAscColumn(this,"${data.anotherTableName}.${data.filedText_eng}")'>↑</a>&nbsp;<a href='#' onclick='$crud.setDescColumn(this,"${data.anotherTableName}.${data.filedText_eng}")'>↓</a></th>
												</#if>
												<#if data.canSort == "否">
													<th>${data.filedText_cn}</th>
												</#if>
											</#list>
										</tr>
										<tbody id="dataTable">
											<tr v-for="data in result">
											<#list tableFiledModels as data>
												<td>{{data.${data.anotherFiledName}}}</td>
											</#list>
											</tr>
										</tbody>
									</table>
									<div id="pageID" class="page_div"></div>
								</div>
								<!-- 查询结果表格显示区域 end-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- 全局js -->
		<script src="../../js/jquery.min.js?v=2.1.4"></script>
		<script src="../../js/bootstrap.min.js?v=3.3.6"></script>

		<!-- 自定义js -->
		<script src="../../js/content.js?v=1.0.0"></script>


		<!-- Bootstrap table -->
		<script src="../../js/plugins/bootstrap-table/bootstrap-table.min.js"></script>
		<script src="../../js/plugins/bootstrap-table/bootstrap-table-mobile.min.js"></script>
		<script src="../../js/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>

		<!-- Peity -->
		<script src="../../js/demo/bootstrap-table-demo.js"></script>
		<script src="../../js/vue/vue.min.js"></script>
		<script src="../../js/util/ajaxFactory.js"></script>
		<script src="../../js/util/crudVueFactory.js"></script>
		<script src="../../js/util/pageMe.js"></script>
		<script src="../../js/config/config.js"></script>
		<script src="../../js/plugins/layer/layer.min.js"></script>
		<script src="../../js/plugins/progressbar/init-mprogress.js"></script>
		<script src="../../js/plugins/progressbar/mprogress.js"></script>

		<script>
			var currentPage = 1;
			var totalPage;
			var sqlMap = {};
			//排序的数据
			var orderData = [];
			var controllerPrefix = "${currentMutiEng}Muti";
			var methodName = "${currentMutiMethodEng}";
			
			var queryVue = new Vue({
				el : '#queryModel',
				data : {
			<#list tableConditionModels as data>
				<#if data.unchangeValue == "">
					<#if data.compareText==">= && <=">
					start${data.anotherTableName}_${data.filed_eng} : '',
					end${data.anotherTableName}_${data.filed_eng} : ''<#if data_has_next>,</#if>
					<#else>
					${data.anotherTableName}_${data.filed_eng} : ''<#if data_has_next>,</#if>
					</#if>
				</#if>
			</#list>
				},
				methods : {
					queryInfo : function() {
						sqlMap = {};
				<#list tableConditionModels as data>
					<#if data.unchangeValue == "">
						<#if data.compareText==">= && <=">
						sqlMap.start${data.anotherTableName}_${data.filed_eng} = this.start${data.anotherTableName}_${data.filed_eng};
						sqlMap.end${data.anotherTableName}_${data.filed_eng} = this.end${data.anotherTableName}_${data.filed_eng};
						<#else>
						sqlMap.${data.anotherTableName}_${data.filed_eng} = this.${data.anotherTableName}_${data.filed_eng};
						</#if>
					</#if>
				</#list>
						currentPage = 1;
						orderData = [];
						$crud.getDataByCurrentPage();
					},
					exportExcel : function() {
	
						//显示进度条
						InitMprogress();
	
	                    var param = '';
	
	                    for (var key in sqlMap) {
	                        param += key + "=" + sqlMap[key] + "&";
	                    }
	
	                    window.location.href = basePath + "/${currentMutiEng}Muti/${currentMutiMethodEng}ExportExcel?" + param;
	
						// 进度条消失
						setTimeout("MprogressEnd()", totalPage / 20 * 1000);
					}
				}
	
			});
			
			
			function makeResult(data) {
				for (var i = 0; i < data.length; i++) {
					if (data[i] == null) {
						data[i] = {};
						<#list tableFiledModels as data>
						data[i]["${data.anotherFiledName}"] = "无";
						</#list>
					}
					<#list tableFiledModels as data>
					<#if data.filedType == "布尔" || data.filedType == "状态码">
						<#list data.filedComment?keys as key>
						if(data[i]["${data.anotherFiledName}"] == "${data.filedComment["${key}"]}") {
							data[i]["${data.anotherFiledName}"] = "${key}";
						}
						</#list>
					</#if>
					</#list>
				}
			}
			
			var tableVue = new Vue({
				el : '#newsContent',
				data : {
					result : []
				},
				//created:
				mounted : function() {
					$crud.getDataByCurrentPage();
				}
			});
			
			//=======================================================vue 2.0
		</script>

	</body>

</html>
</#if>
</#if>