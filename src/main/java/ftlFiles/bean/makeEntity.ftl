package ${packageName}.entity;

<#if ifUseSwagger == "是">
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
</#if>
import java.io.Serializable;
<#--如果className不为空-->
<#if hasList??>
import java.util.List;
</#if>

/**
 * ${makeEntityName_cn}
 */
<#if ifUseSwagger == "是">
@ApiModel
</#if>
public class ${makeEntityName?cap_first} extends CommonEntity implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

<#list MakeEntityModels as makeEntityModel>
    /**
	 *  ${makeEntityModel.filedName_cn}
	 */
	<#if ifUseSwagger == "是">
	@ApiModelProperty(value = "${makeEntityModel.filedName_cn}", name = "${makeEntityModel.filedName_eng}")
	</#if>
    private ${makeEntityModel.serviceType} ${makeEntityModel.filedName_eng};
</#list>

<#list MakeEntityModels as makeEntityModel>
    /**
	 *  get ${makeEntityModel.filedName_cn}
	 */
    public ${makeEntityModel.serviceType} get${makeEntityModel.filedName_eng?cap_first}() {
		return ${makeEntityModel.filedName_eng};
	}

    /**
	 *  set ${makeEntityModel.filedName_cn}
	 */
	public void set${makeEntityModel.filedName_eng?cap_first}(${makeEntityModel.serviceType} ${makeEntityModel.filedName_eng}) {
		this.${makeEntityModel.filedName_eng} = ${makeEntityModel.filedName_eng};
	}
</#list>

}
