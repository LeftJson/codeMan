package ${packageName}.entity;

<#if ifUseSwagger == "是">
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
</#if>
import java.io.Serializable;

/**
 * 生成的实体带有前缀，不必关注，主要是怕不同表查询的字段名称相同，实际开发的时候可以只看后面
 * ${currentMutiEntityNameCn}
 */
<#if ifUseSwagger == "是">
@ApiModel
</#if>
public class ${currentMutiEntityName} extends CommonEntity implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

<#list entityFiledModels as data>
	<#assign javaType = "String">
	<#if data.fieldType == "字符串" || data.fieldType == "文字域" || data.fieldType == "布尔" || data.fieldType == "状态码">
		<#assign javaType = "String">
	<#elseif data.fieldType == "数字">
		<#assign javaType = "Integer">
	<#elseif data.fieldType == "日期">
		<#assign javaType = "java.sql.Timestamp">
	</#if>
	<#if data.compareText==">= && <=">
    /**
	 *  开始${data.fieldName_cn}
	 */
	<#if ifUseSwagger == "是">
	@ApiModelProperty(value = "开始${data.fieldName_cn}", name = "start${data.fieldName}")
	</#if>
    private ${javaType} start${data.fieldName};

    /**
	 *  结束${data.fieldName_cn}
	 */
    <#if ifUseSwagger == "是">
	@ApiModelProperty(value = "结束${data.fieldName_cn}", name = "end${data.fieldName}")
	</#if>
    private ${javaType} end${data.fieldName};
    <#else>

    /**
	 *  ${data.fieldName_cn}
	 */
    <#if ifUseSwagger == "是">
	@ApiModelProperty(value = "${data.fieldName_cn}", name = "${data.fieldName}")
	</#if>
    private ${javaType} ${data.fieldName};
    </#if>
</#list>
<#list entityFiledModels as data>
	<#assign javaType = "String">
	<#if data.fieldType == "字符串" || data.fieldType == "文字域" || data.fieldType == "布尔" || data.fieldType == "状态码">
		<#assign javaType = "String">
	<#elseif data.fieldType == "数字">
		<#assign javaType = "Integer">
	<#elseif data.fieldType == "日期">
		<#assign javaType = "java.sql.Timestamp">
	</#if>

	<#if data.compareText==">= && <=">
    /**
	 *  get 开始${data.fieldName_cn}
	 */
    public ${javaType} getStart${data.fieldName}() {
		return start${data.fieldName};
	}

    /**
	 *  set 开始${data.fieldName_cn}
	 */
	public void setStart${data.fieldName}(${javaType} start${data.fieldName}) {
		this.start${data.fieldName} = start${data.fieldName};
	}
	
	/**
	 *  get 结束${data.fieldName_cn}
	 */
    public ${javaType} getEnd${data.fieldName}() {
		return end${data.fieldName};
	}

    /**
	 *  set 结束${data.fieldName_cn}
	 */
	public void setEnd${data.fieldName}(${javaType} end${data.fieldName}) {
		this.end${data.fieldName} = end${data.fieldName};
	}
	<#else>

	/**
	 *  get ${data.fieldName_cn}
	 */
    public ${javaType} get${data.fieldName?cap_first}() {
		return ${data.fieldName};
	}

    /**
	 *  set ${data.fieldName_cn}
	 */
	public void set${data.fieldName?cap_first}(${javaType} ${data.fieldName}) {
		this.${data.fieldName} = ${data.fieldName};
	}
	</#if>
</#list>
}
