package ${packageName}.entity;

<#if ifUseSwagger == "是">
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
</#if>
import java.io.Serializable;

<#if ifUseSwagger == "是">
@ApiModel
</#if>
public class User implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;
	
	<#if ifUseSwagger == "是">
	@ApiModelProperty(value = "用户名", name = "${userNameFiled}")
	</#if>
	private String ${userNameFiled};

	<#if ifUseSwagger == "是">
	@ApiModelProperty(value = "密码", name = "${userPwdFiled}")
	</#if>
	private String ${userPwdFiled};

	public String get${userNameFiled?cap_first}() {
		return ${userNameFiled};
	}

	public void set${userNameFiled?cap_first}(String ${userNameFiled}) {
		this.${userNameFiled} = ${userNameFiled};
	}

	public String get${userPwdFiled?cap_first}() {
		return ${userPwdFiled};
	}

	public void set${userPwdFiled?cap_first}(String ${userPwdFiled}) {
		this.${userPwdFiled} = ${userPwdFiled};
	}
	
}
