package ${packageName}.entity;

<#if ifUseSwagger == "是">
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
</#if>
import java.io.Serializable;
<#list entityNameAndTypes as nameAndTypeAndComment>
<#--如果className不为空-->
<#if nameAndTypeAndComment.className !="">
import ${nameAndTypeAndComment.className};
</#if>
</#list>

<#if ifUseSwagger == "是">
@ApiModel
</#if>
public class ${entityName} extends CommonEntity implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

<#list entityNameAndTypes as nameAndTypeAndComment>
    /**
	 *  ${nameAndTypeAndComment.comment}
	 */
	<#if ifUseSwagger == "是">
	@ApiModelProperty(value = "${nameAndTypeAndComment.comment}", name = "${nameAndTypeAndComment.sqlParamName}")
	</#if>
    private ${nameAndTypeAndComment.typeName} ${nameAndTypeAndComment.sqlParamName};
</#list>

<#list queryColumnList as data>
    <#assign javaType = "String">
    <#if data.serviceType == "字符串" || data.serviceType == "文字域" || data.serviceType == "布尔" || data.serviceType == "状态码">
        <#assign javaType = "String">
    <#elseif data.serviceType == "数字">
        <#assign javaType = "Integer">
    <#elseif data.serviceType == "日期">
        <#assign javaType = "java.sql.Timestamp">
    </#if>
    <#if data.compareValue==">= && <=">
    /**
	 *  开始${data.columnsCn}
	 */
        <#if ifUseSwagger == "是">
	@ApiModelProperty(value = "开始${data.columnsCn}", name = "start${data.sqlParamColumnEng?cap_first}")
        </#if>
    private ${javaType} start${data.sqlParamColumnEng?cap_first};

    /**
	 *  结束${data.columnsCn}
	 */
        <#if ifUseSwagger == "是">
	@ApiModelProperty(value = "结束${data.columnsCn}", name = "end${data.sqlParamColumnEng?cap_first}")
        </#if>
    private ${javaType} end${data.sqlParamColumnEng?cap_first};
    </#if>
</#list>

<#list entityNameAndTypes as nameAndTypeAndComment>
    /**
	 *  get ${nameAndTypeAndComment.comment}
	 */
    public ${nameAndTypeAndComment.typeName} get${nameAndTypeAndComment.sqlParamName?cap_first}() {
		return ${nameAndTypeAndComment.sqlParamName};
	}

    /**
	 *  set ${nameAndTypeAndComment.comment}
	 */
	public void set${nameAndTypeAndComment.sqlParamName?cap_first}(${nameAndTypeAndComment.typeName} ${nameAndTypeAndComment.sqlParamName}) {
		this.${nameAndTypeAndComment.sqlParamName} = ${nameAndTypeAndComment.sqlParamName};
	}
</#list>

<#list queryColumnList as data>
    <#assign javaType = "String">
    <#if data.serviceType == "字符串" || data.serviceType == "文字域" || data.serviceType == "布尔" || data.serviceType == "状态码">
        <#assign javaType = "String">
    <#elseif data.serviceType == "数字">
        <#assign javaType = "Integer">
    <#elseif data.serviceType == "日期">
        <#assign javaType = "java.sql.Timestamp">
    </#if>
    <#if data.compareValue==">= && <=">
    /**
	 *  get 开始${data.columnsCn}
	 */
    public ${javaType} getStart${data.sqlParamColumnEng?cap_first}() {
		return start${data.sqlParamColumnEng?cap_first};
	}

    /**
	 *  set 开始${data.columnsCn}
	 */
	public void setStart${data.sqlParamColumnEng?cap_first}(${javaType} start${data.sqlParamColumnEng?cap_first}) {
		this.start${data.sqlParamColumnEng?cap_first} = start${data.sqlParamColumnEng?cap_first};
	}

	/**
	 *  get 结束${data.columnsCn}
	 */
    public ${javaType} getEnd${data.sqlParamColumnEng?cap_first}() {
		return end${data.sqlParamColumnEng?cap_first};
	}

    /**
	 *  set 结束${data.columnsCn}
	 */
	public void setEnd${data.sqlParamColumnEng?cap_first}(${javaType} end${data.sqlParamColumnEng?cap_first}) {
		this.end${data.sqlParamColumnEng?cap_first} = end${data.sqlParamColumnEng?cap_first};
	}
    </#if>
</#list>

}
