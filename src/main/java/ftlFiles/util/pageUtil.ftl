package ${packageName}.utils;

import ${packageName}.dao.IBaseDao;

import ${packageName}.entity.PageData;
import java.lang.reflect.*;
import java.util.*;

public class PageUtil {

<#if entityName??>
    public static <E> PageData<E> getPageData(E entity, IBaseDao<E> dao) {

        List<String> orderData = null;
        Integer currentPage = null;
        Class<?> entityClass = entity.getClass();

        try {
            Field orderDataFiled = entityClass.getSuperclass().getDeclaredField("orderData");
            orderDataFiled.setAccessible(true);
            orderData = CastUtil.cast(orderDataFiled.get(entity));

            Field currentPageFiled = entityClass.getSuperclass().getDeclaredField("currentPage");
            currentPageFiled.setAccessible(true);
            currentPage = CastUtil.cast(currentPageFiled.get(entity));
        } catch (Exception e) {
            e.printStackTrace();
        }

        String orderStr = "";

        if (orderData != null) {
            for (int i = 0; i < orderData.size(); i++) {
                if (i == orderData.size() - 1) {
                    orderStr += orderData.get(i);
                    break;
                }
                orderStr += orderData.get(i) + ",";
            }
        }


        <#if dataBaseType == "mysql" || dataBaseType == "postgresql">
		Integer start = (currentPage - 1) * 10;
		</#if>

		<#if dataBaseType == "oracle">
		// 分页开始 页码
		Integer startIndex = (currentPage - 1) * 10 + 1;
		// 分页结束页码
		Integer endIndex = currentPage * 10;
		</#if>


        Integer totalPage = 1;

        try {
			<#if dataBaseType == "mysql" || dataBaseType == "postgresql">
            initEntity(entity, entityClass, orderStr, start);
            </#if>
            <#if dataBaseType == "oracle">
            initEntity(entity, entityClass, orderStr, startIndex, endIndex);
            </#if>
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }


        // 获取总个数
        Integer totalCount = dao.likeSelectCount(entity).intValue();

        List<E> resultList = dao.likeSelect(entity);

        if (totalCount != 0) {

            if (totalCount % 10 == 0) {
                totalPage = totalCount / 10;
            } else {
                totalPage = totalCount / 10 + 1;
            }

        }

        PageData<E> pageData = new PageData<>();

        // 当前页
        pageData.setCurrentPage(currentPage);

        pageData.setCount(totalCount);

        pageData.setTotalPage(totalPage);

        pageData.setResult(resultList);

        return pageData;
    }

<#else>
    public static Map<String, Object> getPageData(Map<String, Object> map, IBaseDao dao) {

        List<String> orderData = CastUtil.cast(map.get("orderData"));

        String orderStr = "";

        if (orderData != null) {
            for (int i = 0; i < orderData.size(); i++) {
                if (i == orderData.size() - 1) {
                    orderStr += orderData.get(i);
                    break;
                }
                orderStr += orderData.get(i) + ",";
            }
        }
        Integer currentPage = (Integer) map.get("currentPage");

        <#if dataBaseType == "mysql" || dataBaseType == "postgresql">
		Integer start = (currentPage - 1) * 10;
		</#if>
		
		<#if dataBaseType == "oracle">
		// 分页开始 页码
		Integer startIndex = (currentPage - 1) * 10 + 1;
		// 分页结束页码
		Integer endIndex = currentPage * 10;
		</#if>

        Integer totalPage = 1;

        <#if dataBaseType == "mysql" || dataBaseType == "postgresql">
		// sql中的start
		map.put("start", start);
		// 每页显示10条
		map.put("pageSize", 10);
		</#if>
		
		<#if dataBaseType == "oracle">
		// sql中的startIndex
		map.put("startIndex", startIndex);
		// sql中的endIndex
		map.put("endIndex", endIndex);
		</#if>

		//排序条件
		map.put("orderStr", orderStr);

        // 获取总个数
        Integer totalCount = dao.likeSelectCount(map).intValue();

        List<Map<String, Object>> resultList = dao.likeSelect(map);

        if (totalCount != 0) {

            if (totalCount % 10 == 0) {
                totalPage = totalCount / 10;
            } else {
                totalPage = totalCount / 10 + 1;
            }

        }

        map.clear();
        // 当前页
        map.put("currentPage", currentPage);

        map.put("count", totalCount);

        map.put("totalPage", totalPage);

        map.put("result", resultList);

        return map;
    }
</#if>

<#if tablesQueryMap??>
	public static <E> PageData<E> getPageData(E entity, Object dao, String methodName) {

        List<String> orderData = null;
        Integer currentPage = null;
        Class<?> entityClass = entity.getClass();

        try {
            Field orderDataFiled = entityClass.getSuperclass().getDeclaredField("orderData");
            orderDataFiled.setAccessible(true);
            orderData = CastUtil.cast(orderDataFiled.get(entity));

            Field currentPageFiled = entityClass.getSuperclass().getDeclaredField("currentPage");
            currentPageFiled.setAccessible(true);
            currentPage = CastUtil.cast(currentPageFiled.get(entity));
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

        String orderStr = "";
        for (int i = 0; i < orderData.size(); i++) {
            if (i == orderData.size() - 1) {
                orderStr += orderData.get(i);
                break;
            }
            orderStr += orderData.get(i) + ",";
        }

        <#if dataBaseType == "mysql" || dataBaseType == "postgresql">
		Integer start = (currentPage - 1) * 10;
		</#if>

		<#if dataBaseType == "oracle">
		// 分页开始 页码
		Integer startIndex = (currentPage - 1) * 10 + 1;
		// 分页结束页码
		Integer endIndex = currentPage * 10;
		</#if>

        Integer totalPage = 1;

        try {
        	<#if dataBaseType == "mysql" || dataBaseType == "postgresql">
            initEntity(entity, entityClass, orderStr, start);
            </#if>
            <#if dataBaseType == "oracle">
            initEntity(entity, entityClass, orderStr, startIndex, endIndex);
            </#if>
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

        Integer totalCount = 0;
        List<E> resultList = null;

        try {
            // 获取总个数
            Method getCountMethod = dao.getClass().getMethod(methodName + "Count", entity.getClass());
            totalCount = ((Long) getCountMethod.invoke(dao, entity)).intValue();

            Method getResultListMethod = dao.getClass().getMethod(methodName, entity.getClass());
            resultList = CastUtil.cast(getResultListMethod.invoke(dao, entity));

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }


        if (totalCount != 0) {

            if (totalCount % 10 == 0) {
                totalPage = totalCount / 10;
            } else {
                totalPage = totalCount / 10 + 1;
            }

        }


        PageData<E> pageData = new PageData<>();

        // 当前页
        pageData.setCurrentPage(currentPage);

        pageData.setCount(totalCount);

        pageData.setTotalPage(totalPage);

        pageData.setResult(resultList);

        return pageData;

    }
</#if>

	private static <E> void initEntity(E entity, Class<?> entityClass, String orderStr, <#if dataBaseType == "mysql" || dataBaseType == "postgresql">Integer start</#if><#if dataBaseType == "oracle">Integer startIndex, Integer endIndex</#if>) throws NoSuchFieldException, IllegalAccessException {
        <#if dataBaseType == "mysql" || dataBaseType == "postgresql">
        	Field startFiled = entityClass.getSuperclass().getDeclaredField("start");
            startFiled.setAccessible(true);
            // sql中的start
            startFiled.set(entity,start);

            Field pageSizeFiled = entityClass.getSuperclass().getDeclaredField("pageSize");
            pageSizeFiled.setAccessible(true);
            // 每页显示10条
            pageSizeFiled.set(entity,10);
        </#if>
        <#if dataBaseType == "oracle">
        	Field startIndexFiled = entityClass.getSuperclass().getDeclaredField("startIndex");
            startIndexFiled.setAccessible(true);
            // sql中的start
            startIndexFiled.set(entity,startIndex);

            Field endIndexFiled = entityClass.getSuperclass().getDeclaredField("endIndex");
            endIndexFiled.setAccessible(true);
            // 每页显示10条
            endIndexFiled.set(entity,endIndex);
        </#if>
            Field orderStrFiled = entityClass.getSuperclass().getDeclaredField("orderStr");
            orderStrFiled.setAccessible(true);
            //排序条件
            orderStrFiled.set(entity, orderStr);
    }

}

