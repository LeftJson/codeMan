package ${packageName}.service;

import ${packageName}.entity.User;

public interface ILoginService {

	User login(User user);

}
