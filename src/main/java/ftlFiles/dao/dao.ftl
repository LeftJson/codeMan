package ${packageName}.dao;

<#if frameWorkVal=="springBoot">
import org.apache.ibatis.annotations.Mapper;
</#if>
import org.springframework.stereotype.Repository;
<#if entityName??>
import ${packageName}.entity.${entityName};
</#if>

<#if frameWorkVal=="springBoot">
@Mapper
</#if>
@Repository
<#if !entityName??>
public interface ${IDaoName} extends IBaseDao {

}
<#else>
public interface ${IDaoName} extends IBaseDao<${entityName}> {

}
</#if>
