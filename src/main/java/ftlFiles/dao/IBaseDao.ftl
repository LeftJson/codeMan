package ${packageName}.dao;

import java.util.List;
<#if !entityName??>
import java.util.Map;
</#if>

<#if !entityName??>
public interface IBaseDao {

	void add(Map<String, Object> map);
	
	void delete(Map<String, Object> map);
	
	void update(Map<String, Object> map);
	
	List<Map<String,Object>> select(Map<String, Object> map);
	
	List<Map<String,Object>> likeSelect(Map<String, Object> map);
	
	Long likeSelectCount(Map<String, Object> map);
	
	void batchAdd(List<Map<String, Object>> list);
	
	void batchDelete(List<Map<String, Object>> list);
	
	void batchUpdate(List<Map<String, Object>> list);
}
<#else>
public interface IBaseDao<E> {

    void add(E map);

    void delete(E map);

    void update(E map);

    List<E> select(E map);

    List<E> likeSelect(E entity);

    Long likeSelectCount(E entity);

    void batchAdd(List<E> list);

    void batchDelete(List<E> list);

    void batchUpdate(List<E> list);
}
</#if>