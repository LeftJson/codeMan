(function(myWindow) {
	<#if theme == "经典后台Thymleaf版">
	myWindow.basePath="/${projectName}";
	</#if>
	<#if theme == "前后端分离响应式">
	myWindow.basePath="http://localhost:8080/${projectName}";
	</#if>
})(window);