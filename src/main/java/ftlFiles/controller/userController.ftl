package ${packageName}.controller;

import javax.servlet.http.HttpSession;
<#if loginModel == "动态用户">
import org.springframework.beans.factory.annotation.Autowired;
</#if>
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ${packageName}.constant.ResultConstant;
import ${packageName}.entity.CommonResult;
import ${packageName}.entity.User;
<#if loginModel == "动态用户">
import ${packageName}.service.ILoginService;
</#if>
<#if ifUseSwagger == "是">
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
</#if>

@RestController
@CrossOrigin(origins = "*",allowCredentials = "true",allowedHeaders = "*")
<#if ifUseSwagger == "是">
@Api(tags = "login接口")
</#if>
@RequestMapping("/login")
public class LoginController {

<#if loginModel == "动态用户">
	private ILoginService service;

	@Autowired
	public LoginController(ILoginService service) {
		this.service = service;
	}
	
	<#if ifUseSwagger == "是">
	@ApiOperation(value = "登录")
	</#if>
	@RequestMapping(value = "/doLogin", method = RequestMethod.POST)
	public CommonResult doLogin(@RequestBody User user, HttpSession session) {

		User currentUser = service.login(user);
		if (currentUser != null) {
			session.setAttribute("user", user);
			return new CommonResult(ResultConstant.SUCCCSS_CODE, ResultConstant.SUCCESS_MSG);
		}

		return new CommonResult(ResultConstant.LOGIN_FAIL_CODE, ResultConstant.FAIL_MSG);

	}
</#if>
	
<#if loginModel == "静态用户">
	
	<#if ifUseSwagger == "是">
	@ApiOperation(value = "登录")
	</#if>
	@RequestMapping(value = "/doLogin", method = RequestMethod.POST)
	public CommonResult doLogin(@RequestBody User user, HttpSession session) {

		if (${userCondition}) {

			session.setAttribute("user", user);
			return new CommonResult(ResultConstant.SUCCCSS_CODE, ResultConstant.SUCCESS_MSG);
		}
		return new CommonResult(ResultConstant.LOGIN_FAIL_CODE, ResultConstant.FAIL_MSG);

	}
</#if>
	
	<#if ifUseSwagger == "是">
	@ApiOperation(value = "退出登录")
	</#if>
	@RequestMapping(value = "/doLogOut", method = RequestMethod.POST)
	public CommonResult doLogOut(HttpSession session) {

		session.removeAttribute("user");
		return new CommonResult(ResultConstant.SUCCCSS_CODE, ResultConstant.SUCCESS_MSG);

	}

}
