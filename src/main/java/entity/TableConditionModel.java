package entity;

import javax.swing.JComboBox;
import javax.swing.JTextField;

import constant.CompareComboBox;
import constant.FiledComment;
import constant.Filed_cn;
import constant.Filed_eng;
import constant.RelationJcombox;
import constant.ServiceTypeComboBox;

public class TableConditionModel {

	private RelationJcombox<String> relationJcombox;

	private JComboBox<String> comboBox;

	private Filed_eng filed_eng_textFiled;

	private Filed_cn filed_cn_textFiled;

	private ServiceTypeComboBox<String> serviceTypeComboBox;

	private FiledComment filedComment;

	private CompareComboBox<String> compareComboBox;

	private JTextField textFiled;

	private String relation;

	private String tableName;

	private String anotherTableName;

	private String filed_eng;

	private String filed_cn;

	private String serviceType;

	private Object serviceText;

	private String compareText;

	private String unchangeValue;

	public RelationJcombox<String> getRelationJcombox() {
		return relationJcombox;
	}

	public void setRelationJcombox(RelationJcombox<String> relationJcombox) {
		this.relationJcombox = relationJcombox;
	}

	public JComboBox<String> getComboBox() {
		return comboBox;
	}

	public void setComboBox(JComboBox<String> comboBox) {
		this.comboBox = comboBox;
	}

	public Filed_eng getFiled_eng_textFiled() {
		return filed_eng_textFiled;
	}

	public void setFiled_eng_textFiled(Filed_eng filed_eng_textFiled) {
		this.filed_eng_textFiled = filed_eng_textFiled;
	}

	public Filed_cn getFiled_cn_textFiled() {
		return filed_cn_textFiled;
	}

	public void setFiled_cn_textFiled(Filed_cn filed_cn_textFiled) {
		this.filed_cn_textFiled = filed_cn_textFiled;
	}

	public CompareComboBox<String> getCompareComboBox() {
		return compareComboBox;
	}

	public void setCompareComboBox(CompareComboBox<String> compareComboBox) {
		this.compareComboBox = compareComboBox;
	}

	public JTextField getTextFiled() {
		return textFiled;
	}

	public void setTextFiled(JTextField textFiled) {
		this.textFiled = textFiled;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getAnotherTableName() {
		return anotherTableName;
	}

	public void setAnotherTableName(String anotherTableName) {
		this.anotherTableName = anotherTableName;
	}

	public String getFiled_eng() {
		return filed_eng;
	}

	public void setFiled_eng(String filed_eng) {
		this.filed_eng = filed_eng;
	}

	public String getFiled_cn() {
		return filed_cn;
	}

	public void setFiled_cn(String filed_cn) {
		this.filed_cn = filed_cn;
	}

	public ServiceTypeComboBox<String> getServiceTypeComboBox() {
		return serviceTypeComboBox;
	}

	public void setServiceTypeComboBox(ServiceTypeComboBox<String> serviceTypeComboBox) {
		this.serviceTypeComboBox = serviceTypeComboBox;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public FiledComment getFiledComment() {
		return filedComment;
	}

	public void setFiledComment(FiledComment filedComment) {
		this.filedComment = filedComment;
	}

	public Object getServiceText() {
		return serviceText;
	}

	public void setServiceText(Object serviceText) {
		this.serviceText = serviceText;
	}

	public String getCompareText() {
		return compareText;
	}

	public void setCompareText(String compareText) {
		this.compareText = compareText;
	}

	public String getUnchangeValue() {
		return unchangeValue;
	}

	public void setUnchangeValue(String unchangeValue) {
		this.unchangeValue = unchangeValue;
	}

	@Override
	public String toString() {
		return "TableConditionModel [relation=" + relation + ", tableName=" + tableName + ", anotherTableName="
				+ anotherTableName + ", filed_eng=" + filed_eng + ", filed_cn=" + filed_cn + ", compareText="
				+ compareText + ", unchangeValue=" + unchangeValue + "]";
	}

}
