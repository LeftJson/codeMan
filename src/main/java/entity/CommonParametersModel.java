package entity;

public class CommonParametersModel {
	
	private String databasePool = "HikariCP";
	
	private String ifUseSwagger = "是";

	public String getDatabasePool() {
		return databasePool;
	}

	public void setDatabasePool(String databasePool) {
		this.databasePool = databasePool;
	}

	public String getIfUseSwagger() {
		return ifUseSwagger;
	}

	public void setIfUseSwagger(String ifUseSwagger) {
		this.ifUseSwagger = ifUseSwagger;
	}
	
	

}
