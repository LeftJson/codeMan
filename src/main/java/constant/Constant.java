package constant;

import javax.swing.JFrame;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class Constant {

	public static JFrame frmv;

	public static final String projectName = "freeOut";

	public static final String modelFiles = "C:\\codeManConfig\\modelFiles\\";

	public static final String modelFilesZip = "C:\\codeManConfig\\modelFiles\\model.zip";

	public static final String staticZip = "C:\\codeManConfig\\modelFiles\\static.zip";
	
	public static final String webClientZip = "C:\\codeManConfig\\modelFiles\\webClient.zip";

	public static final String mvcSettingsZip = "C:\\codeManConfig\\modelFiles\\mvcSettings.zip";

	public static final String settingsZip = "C:\\codeManConfig\\modelFiles\\settings.zip";

	public static final String libZip = "C:\\codeManConfig\\modelFiles\\lib.zip";

	public static final String zipPwd = "beautiful123,,#@";

	// 获取激活文件
	public static final String lifePropertiesPath = "C:\\codeManConfig\\codeManSdk\\bin\\server\\life.properties";

	// 项目根目录
	public static final String freeOutPath = "\\freeOut\\";

	public static final String freeOutPath_lib_MVC = "\\freeOut\\src\\main\\webapp\\WEB-INF\\lib\\";

	public static final String freeOutPath_lib = "\\freeOut\\lib\\";

	public static final String freeOutPath_METAINF_MVC = "\\freeOut\\src\\main\\webapp\\META-INF\\";

	public static final String mainApplicationPath = "\\freeOut\\src\\main\\java\\freeOut\\";

	public static final String testPath = "\\freeOut\\src\\test\\java\\freeOut\\";

	public static final String freeOutPath_controller = "\\freeOut\\src\\main\\java\\freeOut\\controller\\";

	public static final String freeOutPath_service = "\\freeOut\\src\\main\\java\\freeOut\\service\\";

	public static final String freeOutPath_serviceImpl = "\\freeOut\\src\\main\\java\\freeOut\\service\\impl\\";

	public static final String freeOutPath_dao = "\\freeOut\\src\\main\\java\\freeOut\\dao\\";

	public static final String freeOutPath_entity = "\\freeOut\\src\\main\\java\\freeOut\\entity\\";
	
	public static final String freeOutPath_constant = "\\freeOut\\src\\main\\java\\freeOut\\constant\\";

	public static final String freeOutPath_resources = "\\freeOut\\src\\main\\resources\\";

	public static final String freeOutPath_sqlMapper = "\\freeOut\\src\\main\\resources\\sqlmapper\\";

	public static final String freeOutPath_sqlMapper_MVC = "\\freeOut\\src\\main\\java\\freeOut\\sqlmapper\\";

	public static final String freeOutPath_static = "\\freeOut\\src\\main\\resources\\static\\mystatic\\";

	public static final String freeOutPath_static_MVC = "\\freeOut\\src\\main\\webapp\\mystatic\\";

	public static final String freeOutPath_templates = "\\freeOut\\src\\main\\resources\\templates\\";

	public static final String freeOutPath_WEBINF_MVC = "\\freeOut\\src\\main\\webapp\\WEB-INF\\";

	public static String downSettingsZipNet = "/zip/settings.zip";

	public static String downMvcSettingsZipNet = "/zip/mvcSettings.zip";

	public static final String freeOutPath_settings = "\\freeOut\\.settings\\";

	public static String downStaticZipNet = "/zip/static.zip";
	
	public static String downWebClientZipNet = "/zip/webClient.zip";

	public static String downLibZipNet = "/zip/lib.zip";

	public static final String utilPath = "\\freeOut\\src\\main\\java\\freeOut\\utils\\";

	public static final String mvcConfig = "\\freeOut\\src\\main\\java\\freeOut\\config\\";

	public static String everythingZip = "/zip/everything.zip";

	public static final String everythingDownLoadDir = "C:\\codeManConfig\\everything\\";

	public static final String everythingExeName = "C:\\codeManConfig\\everything\\Everything.exe";

	public static String dubboModelZip = "/zip/dubboModel.zip";

	public static String cloudModelZip = "/zip/cloudModel.zip";

    public static final String gitIpAdressFilePath = "C:\\codeManConfig\\codeManIp\\ip.txt";

    public static final String gitConfigFilePath = "C:\\codeManConfig\\.git";
    
    public static final String webClientPath = "\\freeOut-webClient\\";

	public static final String webClientHtmlPath = "\\freeOut-webClient\\views\\";
	
	public static final String webClientJsConfigPath = "\\freeOut-webClient\\js\\config\\";
	

	static {
        File ipFile = new File(Constant.gitIpAdressFilePath);
        try (BufferedReader reader = new BufferedReader(new FileReader(ipFile))){
            String ipPath = reader.readLine();

            Constant.downSettingsZipNet = ipPath + Constant.downSettingsZipNet;
            Constant.downMvcSettingsZipNet = ipPath + Constant.downMvcSettingsZipNet;
            Constant.downStaticZipNet = ipPath + Constant.downStaticZipNet;
            Constant.downWebClientZipNet = ipPath + Constant.downWebClientZipNet;
            Constant.downLibZipNet = ipPath + Constant.downLibZipNet;
            Constant.everythingZip = ipPath + Constant.everythingZip;
            Constant.dubboModelZip = ipPath + Constant.dubboModelZip;
            Constant.cloudModelZip = ipPath + Constant.cloudModelZip;

        } catch (Exception e) {
        }
	}


}
